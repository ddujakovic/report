import pprint
import ssl
import itertools
import operator

from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim


class IaaSReportVMW(object):
    """Retrival and parsing IaaS data from vSphere vCenter"""

    service_instance = None
    hosts = None
    hostPgDict = None

    def __init__(self, host, user, pwd, port):
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        ssl_context.verify_mode = ssl.CERT_NONE

        self.service_instance = connect.SmartConnect(host=host,
                                                     user=user,
                                                     pwd=pwd,
                                                     port=int(port),
                                                     sslContext=ssl_context)
    ####################
    ###GET VM SUMMARY###
    ####################
    def get_vms(self):
        """ get_vms """
        content = self.service_instance.RetrieveContent()

        container = content.rootFolder  # starting point to look into
        viewType = [vim.VirtualMachine]  # object types to look for
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive)

        children = containerView.view

        vmsout = []
        for virtual_machine in children:
            summary = virtual_machine.summary
            if hasattr(virtual_machine.resourcePool, 'name'):
                rpool = virtual_machine.resourcePool.name
            else:
                rpool = "root"

            folder = None
            if str(virtual_machine.parent).startswith("'vim.Folder"):
                folder = virtual_machine.parent.name
                vmparent = virtual_machine.parent.parent
                # rekurzivno tražiti dok parent folder nije 'vm'
                while folder != 'vm' and vmparent.name != 'vm':
                    folder = vmparent.name
                    vmparent = vmparent.parent
            else:
                # vApp je oblik resource poola
                if hasattr(virtual_machine.resourcePool, 'parentFolder'):
                    folder = virtual_machine.resourcePool.parentFolder.name
                else:
                    print("no folder, using resource pool name for KK ID! %s %s" % (
                        rpool, summary.config.name))
                    pprint.pprint(virtual_machine.resourcePool.parent)

            storage = summary.storage
            committed = None
            uncommitted = None
            if storage:
                committed = summary.storage.committed
                uncommitted = summary.storage.uncommitted
            vmsout.append({
                'uuid': summary.config.instanceUuid,
                'vm': summary.config.name,
                'rp': rpool,
                'power_state': summary.runtime.powerState,
                'ram': summary.config.memorySizeMB,
                'vcpu': summary.config.numCpu,
                'folder': folder,
                'annotation': summary.config.annotation,
                'storage': committed,
                'storage_uncommited': uncommitted,
                'os_type': summary.config.guestFullName
            })

        vmsout = sorted(vmsout, key=lambda k: k['rp'])
        return vmsout

    ######################
    ###GET VM SNAPSHOTS###
    ######################
    def get_snapshots(self): 
        """ get_snapshots """
        content = self.service_instance.RetrieveContent()

        container = content.rootFolder  # starting point to look into
        viewType = [vim.VirtualMachine]  # object types to look for
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive)

        children = containerView.view
        #print(children) vim.VirtualMachine:vm-486455
        snapshotsout = []
        for virtual_machine in children:
            summary = virtual_machine.summary #vim.VirtualMachine:vm-486455.summary
            #print(summary) VirtualMachineSummary(vim.vm.Summary)
            #print(virtual_machine.)
            if hasattr(virtual_machine.resourcePool, 'name'):
                rpool = virtual_machine.resourcePool.name
            else:
                rpool = "root"

            folder = None
            if str(virtual_machine.parent).startswith("'vim.Folder"):
                folder = virtual_machine.parent.name
                vmparent = virtual_machine.parent.parent
                # rekurzivno tražiti dok parent folder nije 'vm'
                while folder != 'vm' and vmparent.name != 'vm':
                    folder = vmparent.name
                    vmparent = vmparent.parent
            else:
                # vApp je oblik resource poola
                if hasattr(virtual_machine.resourcePool, 'parentFolder'):
                    folder = virtual_machine.resourcePool.parentFolder.name
                else:
                    print("no folder, using resource pool name for KK ID! %s %s" % (
                        rpool, summary.config.name))
                    pprint.pprint(virtual_machine.resourcePool.parent)

            snapshotsout.append({
                'uuid': summary.config.instanceUuid,
                'vm': summary.config.name,
                'rp': rpool,
                'power_state': summary.runtime.powerState,
                'folder': folder,
				'snapshot': vim.vm.SnapshotInfo.currentSnapshot
            })

        snapshotsout = sorted(snapshotsout, key=lambda k: k['rp'])
        return snapshotsout
    ####################
    ###GET DATASTORES###
    ####################
    def get_datastores(self):
            """ get_datastores """
            content = self.service_instance.RetrieveContent()

            container = content.rootFolder  # starting point to look into
            viewType = [vim.Datastore]  # object types to look for
            recursive = True  # whether we should look into it recursively
            containerView = content.viewManager.CreateContainerView(
                container, viewType, recursive)

            children = containerView.view

            dsout = []
            outname = {}
            for datastore in children:
                summary = datastore.summary
                # prekoči duple
                if not summary.name in outname:
                    dsout.append({
                        'name': summary.name,
                        'capacity': summary.capacity/1024**3,
                        'free': summary.freeSpace/1024**3
                    })
                    outname[summary.name] = 1

            dsout = list(self.sort_uniq(dsout, 'name'))
            return dsout
    ####################
    ###GET ESXI HOSTS###
    ####################
    def get_hosts(self, content):
        host_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                            [vim.HostSystem],
                                                            True)
        obj = [host for host in host_view.view]
        host_view.Destroy()
        return obj

    ####################
    ###GET ESXI PORTS###
    ####################
    def get_portgroups(self, hosts, vlans):
        for host in hosts:
            pgs = host.config.network.portgroup
            for p in pgs:
                vlans[p.spec.name] = p.spec.vlanId

    ####################
    ###GET VMWARE VLANS###
    ####################
    def get_vlans(self):
        vlans = {}

        content = self.service_instance.RetrieveContent()
        container = content.rootFolder  # starting point to look into
        # object types to look for
        viewType = [vim.dvs.DistributedVirtualPortgroup]
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive)

        children = containerView.view
        for dvs in children:
            if isinstance(dvs.config.defaultPortConfig.vlan, vim.dvs.VmwareDistributedVirtualSwitch.VlanIdSpec):
                vlans[dvs.config.name] = dvs.config.defaultPortConfig.vlan.vlanId
            elif isinstance(dvs.config.defaultPortConfig.vlan,
                            vim.dvs.VmwareDistributedVirtualSwitch.TrunkVlanSpec):
                vnr = dvs.config.defaultPortConfig.vlan.vlanId
                vlan = ""
                for nr in vnr:
                    if vlan:
                        vlan = "%s, " % vlan
                    if nr.start == nr.end:
                        vlan = "%s%s" % (vlan, nr.start)
                    else:
                        vlan = "%s%s-%s" % (vlan, nr.start, nr.end)
                vlans[dvs.config.name] = vlan

        # vSwitch
        hosts = self.get_hosts(content)
        self.get_portgroups(hosts, vlans)
        return vlans

    def sort_uniq(self, sequence, key):
        return map(
            operator.itemgetter(0),
            itertools.groupby(sorted(sequence, key=lambda k: k[key])))

    def disconnect(self):
        """ disconnect """
        connect.Disconnect(self.service_instance)
