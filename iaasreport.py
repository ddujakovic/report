#!/usr/bin/env python3
""" iaas reporting - RAM, CPU, disk """
import time
import traceback

#import iaasreporlpar
import iaasreportvmw
import xlsreport
import iaasreporthcs
#import iaasreportfujitsu
#import iaasreportlinux
import config
import korisnik
import xlscapacity


def vmware(xls, kk, xlscap):
    """ vmware vms and datastores """

    for addr, desc in config.cfg.vcenters.items():
        print(desc)
        try:
            vmw = iaasreportvmw.IaaSReportVMW(addr,config.cfg.USERNAME,config.cfg.PASSWORD,443)
        
            vms = vmw.get_vms()
            xls.save_vmw_vms(vms, desc)
            kk.add_vmw_vms(vms, desc)
            vlans = vmw.get_vlans()
            kk.add_vmw_vlans(vlans, desc)

            datastores = vmw.get_datastores()
            xls.save_vmw_datastores(datastores, desc)
            kk.add_vmw_datastores(datastores, desc)

            vmw.disconnect()
        except:
            print("ERROR vmware:", desc)


def hcs_non_vmw(xls, kk, xlscap):
    """ HCS non vmw LUNs """
    hcs = iaasreporthcs.IaaSReportHCS()

    hcs.gethosts()

    storages = hcs.getarrays()
    for stor in storages:
        print("stor %s" % stor)
        try:
            luns = hcs.getluns(stor['arrayType'], stor['serialNumber'])
            xls.save_non_vmw_luns(luns,
                                  stor['displayArrayType'],
                                  stor['serialNumber']
                                 )
            kk.add_non_vmw_luns(luns,
                                stor['displayArrayType'],
                                stor['serialNumber'])
        except:
            traceback.print_stack()

def main():
    """ main """

    filename = "iaasreport-%s.xlsx" % time.strftime("%Y%m%d-%H%M%S")
    capfilename = "capacity-%s.xlsx" % time.strftime("%Y%m%d-%H%M%S")
    xls = xlsreport.XLSReport(filename)
    xlscap = xlscapacity.XLSCapacity(capfilename)
    kk = korisnik.Korisnik("korisnici.html")

    print('VMWARE KORISNICI \n')
    try:
        vmware(xls, kk, xlscap)
    except:
        traceback.print_stack()

    print('HCS LUNS \n')
    try:
        hcs_non_vmw(xls, kk, xlscap)
    except:
        traceback.print_stack()

    xls.close()

    #capacity report
    kk.save_xls(xlscap)
    xlscap.close()
    kk.gen_xlsx_index()

    kk.save_html(time.strftime("%Y%m%d-%H%M%S"))


if __name__ == "__main__":
    main()
