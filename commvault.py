import requests
import sys
import xml.etree.ElementTree as ET
import base64
import json
import mysql.connector
from datetime import datetime
import config
import re
import urllib3
###Disable HTTPS warnings https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )

time=datetime.now()
time=time.strftime("%d.%m.%Y %H:%M")
re_kid = re.compile(r'.*(K\d\d\d\d)')

user = config.cfg.cmvltuser
pwd = config.cfg.cmvltpass

base_url = "http://100.64.4.34:81/SearchSvc/CVWebService.svc"
################LOGIN######################
global login_token
url = "https://100.64.4.34/webconsole/api/Login"
payload = {
    "username": user,
    "password": pwd
    }
headers = {"Accept": "application/json","Content-type": "application/json"}
response = requests.request("POST", url, json=payload, headers=headers, verify=False)
response = json.loads(response.text)
login_token = response['token']
#####################
def prepare_tables(mycursor):
    ###############
    ###commvault###
    ###############
    mycursor.execute("CREATE TABLE IF NOT EXISTS commvault (kid VARCHAR(255),  tenant VARCHAR(255), client_id INT PRIMARY KEY, lic_group VARCHAR(255), lic_detail VARCHAR(255))")
class commv(object):
    def client_id(self):
        url = "https://100.64.4.34/webconsole/api/ClientGroup"

        payload = ""
        headers = {"Accept": "application/json","Authtoken": login_token}

        response = requests.request("GET", url, data=payload, headers=headers, verify=False)
        #print(response.text)
        response = json.loads(response.text)
        allclients=(response['groups'])
        dataout=[]
        for index in range(len(allclients)):
            for key in allclients[index]:
                if key == 'Id':
                    clientid=str(allclients[index]['Id'])
                    url = "https://100.64.4.34/webconsole/api/ClientGroup/"+clientid
                    payload = ""
                    headers = {"Accept": "application/json","Authtoken": login_token}
                    response = requests.request("GET", url, data=payload, headers=headers, verify=False)
                    response = json.loads(response.text)
                    #description=str(response['clientGroupDetail']['description'])
                    m = re_kid.match(response['clientGroupDetail']['description'])
                    kid = None
                    if m:
                        array = {
                            'ClientName': str(allclients[index]['name']),
                            'ClientID': str(allclients[index]['Id']),
                            'CommvaultGroup': str(response['clientGroupDetail']['clientGroup']['clientGroupName']),
                            'Description': str(response['clientGroupDetail']['description'])
                        }
                        dataout.append(array)
        #print(dataout)
        return dataout

    def clientdetails(self):
        dataout=[]
        url = "https://100.64.4.34/webconsole/api/Client"
        payload = ""
        headers = {"Accept": "application/json","Authtoken": login_token}
        response = requests.request("GET", url, data=payload, headers=headers, verify=False)
        response = json.loads(response.text)
        agents=(response['clientProperties'])

        for index in range(len(agents)):
            clientid=str(agents[index]['client']['clientEntity']['clientId'])
            url = "https://100.64.4.34/webconsole/api/Client/"+clientid
            payload = ""
            headers = {"Accept": "application/json","Authtoken": login_token}
            response = requests.request("GET", url, data=payload, headers=headers, verify=False)
            response = json.loads(response.text)
            try:
                license = (response['clientProperties'][0]['AdvancedFeatures'])
                res = []
                for indeks in range(len(license)):
                    res.append(str(license[indeks]['LicenseName']))
            except KeyError:
                continue
            try:
                array = {
                    'clientDisplayName': str(response['clientProperties'][0]['client']['displayName']),
                    'clientOSName': str(response['clientProperties'][0]['client']['osInfo']['OsDisplayInfo']['OSName']),
                    'clientCommvaultGroupId': str(response['clientProperties'][0]['clientGroups'][0]['clientGroupId']),
                    'isdeletedclient': str(response['clientProperties'][0]['clientProps']['IsDeletedClient']),
                    'clientLicenses': str(res)
                }
                dataout.append(array)
            except KeyError:
                continue
        #print(dataout)
        return dataout

    def commvaultlibraries(self):
        dataout=[]
        url = "https://100.64.4.34/webconsole/api/Library"
        payload = ""
        headers = {"Accept": "application/json","Authtoken": login_token}
        response = requests.request("GET", url, data=payload, headers=headers, verify=False)
        response = json.loads(response.text)
        libraries=(response['response'])
        #print(libraries)
        for index in range(len(libraries)):
            #for key in libraries[index]:
            #    if key == 'id':
            array = {
                    'LibraryName': str(libraries[index]['entityInfo']['name']),
                    'LibraryID': str(libraries[index]['entityInfo']['id'])
                }
            dataout.append(array)
        #print(dataout)
        return dataout

    def commvaultlibraryusage(self,libraries):
        dataout=[]
        libraryID=libraries
        for index in range(len(libraryID)):
            libID=str(libraryID[index]['LibraryID'])
            #print(libID)
            url = "https://100.64.4.34/webconsole/api/Library/"+libID
            payload = ""
            headers = {"Accept": "application/json","Authtoken": login_token}
            response = requests.request("GET", url, data=payload, headers=headers, verify=False)
            response = json.loads(response.text)
            libusage=(response['libraryInfo'])
            #print(libusage)
            for index in range(len(libusage)):
                kid=str(libusage['library']['libraryName'])
                kid=kid[-5:]
                #if kid==re_kid.match(kid):
                array = {
                        'kid': kid,
                        'LibraryName': str(libusage['library']['libraryName']),
                        'LibraryID': str(libusage['library']['libraryId']),
                        'LibraryTotal': str(libusage['magLibSummary']['totalCapacity']).strip(),
                        'LibraryAvailable': str(libusage['magLibSummary']['totalAvailableSpace']).strip()
                    }
            dataout.append(array)
        #print(dataout)
        return dataout
