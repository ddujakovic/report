###########
#OPTIMIZED 04/2023
###########
import os
import subprocess
import requests
import json
import urllib3
import config
###Disable HTTPS warnings https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

tokenID = requests.post(
    "https://100.64.58.199:9398/api/sessionMngr/?v=latest", #TCP 9398 Veeam enterprise manager restAPI
    headers={
        "Accept": "application/json",
        "Authorization": config.cfg.veeamentapi,
    },
    verify=False,
).headers["x-restsvcsessionid"]

url = "https://100.64.58.10:9419/api/oauth2/token" #TCP 9419 veeam B&R restAPI port
payload = {
    "grant_type": "password",
    "username": config.cfg.veeambackupuser,
    "password": config.cfg.veeambackuppass,
}
headers = {
    "x-api-version": "1.0-rev1",
    "Content-Type": "application/x-www-form-urlencoded",
}
response = requests.post(url, data=payload, headers=headers, verify=False)
tokenIDBandR = response.json()["access_token"]

class veeamvspp(object):
    #svi backup jobovi
    def getveeamvsppjobs(self):
        jobsout = []
        url = "https://vssp.datacloud.loc:9398/api/jobs"
        headers = {
            "Accept": "application/json",
            "X-RestSvcSessionId": tokenID,
        }
        response = requests.get(url, headers=headers, verify=False)
        if response.status_code == 200:
            for job in response.json()["Refs"]:
                if "Name" in job:
                    jobsout.append({"JobName": job["Name"]})
        else:
            print(f"Error getting Veeam jobs: {response.text}")
        return jobsout



    def getveeamvsppbackupfiles(self):
        url = "https://vssp.datacloud.loc:9398/api/backupFiles/"
        headers = {
            "Accept": "application/json",
            "X-RestSvcSessionId": tokenID,
        }
        response = requests.get(url, headers=headers, verify=False)
        #response.raise_for_status()

        backup_files = response.json()["Refs"]
        veeambackupfilesout = []
        for file in backup_files:
            backup_file_out = {"BackupFile": f"{file['Href']}?format=Entity"}
            veeambackupfilesout.append(backup_file_out)
        return veeambackupfilesout
        
    def getbackupfiledetails(self,veeambackupfilesout):
        backupfilesout = []
        headers = {
            "Accept": "application/json",
            "X-RestSvcSessionId": tokenID,
        }

        for file in veeambackupfilesout:
            backupfile = file['BackupFile']
            response = requests.get(backupfile, headers=headers, verify=False)
            bckfile = response.json()

            for key, value in bckfile.items():
                if key == 'FilePath':
                    backupfilesout.append({
                        'FilePath': value,
                        'Name': bckfile['Name'],
                        'BackupSize': bckfile['BackupSize']
                    })

        return backupfilesout

    def getveeambackupid(self):
        url = "https://100.64.58.10:9419/api/v1/backups"
        
        headers = {
            "x-api-version": "1.0-rev1",
            "Accept": "application/json",
            "Authorization": "Bearer "+tokenIDBandR
        }
        
        response = requests.get(url, headers=headers, verify=False)
        veeamjobsid = response.json()['data']
        
        jobid=[]
        for index in range(len(veeamjobsid)):
            for key in veeamjobsid[index]:
                if key == 'id':
                    array = {
                        'JobID': str(veeamjobsid[index]['id']),
                        'Name': str(veeamjobsid[index]['name'])
                    }
                    jobid.append(array)
        #print(jobid)

        return jobid

    def getveeambackupvmcount(self,backupjobs):
        dataout = [{
            'JobName': str(job['Name']),
            'VMCount': str(requests.get(
                f"https://100.64.58.10:9419/api/v1/backups/{job['JobID']}/objects",
                headers={
                    "x-api-version": "1.0-rev1",
                    "Accept": "application/json",
                    "Authorization": f"Bearer {tokenIDBandR}"
                },
                verify=False
            ).json()['pagination']['total'])
        } for job in backupjobs]
        return dataout

#def main():
#    backupjobs=veeamvspp.getveeambackupid()
#    veeamvspp.getveeambackupvmcount(backupjobs)

#if __name__ == "__main__":
#    main()