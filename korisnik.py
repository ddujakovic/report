import re
import sys

import os
from operator import itemgetter

import config


class Korisnik(object):
    """ korisnik """

    def __init__(self, filename):
        self.filename = filename
        self.kks = {}
        self.re_kid = re.compile(r'.*(K\d\d\d\d)')
        self.fd = None

    def add_vmw_vms(self, vms, vcenter):
        for item in vms:
            # item['folder'] za suffix ima ID korisnika, Kxxxx
            # šifra u folderu umjesto resource poolu
            m = self.re_kid.match(item['folder'])
            if not m:
                # nema korisnik ID suffix u imenu
                continue
            kkid = m.group(1)

            if not kkid in self.kks:
                # prvi put se spominje kkid
                self.kks[kkid] = {}

            if not 'vms' in self.kks[kkid]:
                self.kks[kkid]['vms'] = {}

            if not vcenter in self.kks[kkid]['vms']:
                # prvi put se spominje 'vms'
                self.kks[kkid]['vms'][vcenter] = []

            self.kks[kkid]['vms'][vcenter].append(item)

    def add_vmw_datastores(self, dts, vcenter):
        for item in dts:
            # item['name'] za suffix ima ID korisnika, Kxxxx
            m = self.re_kid.match(item['name'])
            if not m:
                # nema korisnik ID suffix u imenu
                continue
            kkid = m.group(1)

            if not kkid in self.kks:
                # prvi put se spominje kkid
                self.kks[kkid] = {}

            if not 'dts' in self.kks[kkid]:
                self.kks[kkid]['dts'] = {}

            # 'dts' je key za vmware datastore
            if not vcenter in self.kks[kkid]['dts']:
                # prvi put se spominje 'dts'
                self.kks[kkid]['dts'][vcenter] = []

            self.kks[kkid]['dts'][vcenter].append(item)

    def add_non_vmw_luns(self, luns, storage, sn):
        for item in luns:
            # item['label'] za suffix ima ID korisnika, Kxxxx
            m = self.re_kid.match(item['label'])
            if not m:
                # nema korisnik ID suffix u imenu
                continue
            kkid = m.group(1)

            if not kkid in self.kks:
                # prvi put se spominje kkid
                self.kks[kkid] = {}

            if not 'lun' in self.kks[kkid]:
                # prvi put se spominje 'lun'
                self.kks[kkid]['lun'] = {}
                self.kks[kkid]['lun'][storage] = {}
                self.kks[kkid]['lun'][storage][sn] = []

            self.kks[kkid]['lun'][storage][sn].append(item)

    def add_vmw_vlans(self, vlans, vcenter):
        vs = {}
        for name in vlans:
            # vid portgrupa name, suffix ima ID korisnika, Kxxxx
            m = self.re_kid.match(name)
            if not m:
                # nema korisnik ID suffix u imenu
                continue
            kkid = m.group(1)

            if not kkid in self.kks:
                # prvi put se spominje kkid
                self.kks[kkid] = {}

            if not 'vnics' in self.kks[kkid]:
                self.kks[kkid]['vnics'] = {}

            if not vcenter in self.kks[kkid]['vnics']:
                # prvi put se spominje 'vnics'
                self.kks[kkid]['vnics'][vcenter] = {}

            self.kks[kkid]['vnics'][vcenter][name] = vlans[name]

    def add_lpars(self, lpars):
        for item in lpars:
            # item['naziv'] za suffix ima ID korisnika, Kxxxx
            m = self.re_kid.match(lpars[item]['naziv'])
            if not m:
                # nema korisnik ID suffix u imenu
                continue
            kkid = m.group(1)

            if not kkid in self.kks:
                # prvi put se spominje kkid
                self.kks[kkid] = {}

            if not 'lpar' in self.kks[kkid]:
                # prvi put se spominje 'lun'
                self.kks[kkid]['lpar'] = []

            self.kks[kkid]['lpar'].append(lpars[item])

    def save_html_vms(self, vms):
        self.fd.write("""
        <div class="sectitle">
            <div>VMware VMs</div> 
            <div>
        """)

        for vcenter in vms:
            self.fd.write("""
            <div class="subsectitle">vCenter: %s</div>
            <div class="data">
                <table>
                    <tr class="dataheader">
                        <th>resource pool</th>
                        <th>name</th>
                        <th>RAM GB</th>
                        <th>vCPU</th>
                        <th>power state</th>
                        <th>folder</th>
                        <th>annotation</th>
                    </tr>
            """ % (vcenter))
            sum_cpu_on = 0
            sum_cpu_off = 0
            sum_ram_on = 0
            sum_ram_off = 0
            for v in vms[vcenter]:
                power_class = ''
                if not v['ram-mb']:
                    v['ram-mb'] = 0

                if v['power-state'] == 'poweredOff':
                    power_class = 'off'
                    if v['cpu']:
                        sum_cpu_off += int(v['cpu'])
                    if v['ram-mb']:
                        sum_ram_off += (int(v['ram-mb'])/1024)
                else:
                    sum_cpu_on += int(v['cpu'])
                    sum_ram_on += (int(v['ram-mb']) / 1024)
                self.fd.write("<tr class=\"datatr%s\">" % power_class)
                self.fd.write("<td>%s</td>" % v['resource-pool'])
                self.fd.write("<td>%s</td>" % v['name'])
                self.fd.write("<td>%s</td>" % (int(v['ram-mb'])/1024))
                self.fd.write("<td>%s</td>" % v['cpu'])
                self.fd.write("<td>%s</td>" % v['power-state'])
                self.fd.write("<td>%s</td>" % v['folder'])
                self.fd.write("<td>%s</td>" % v['annotation'])
                self.fd.write("</tr>")
            sum_ram_off = round(sum_ram_off)
            sum_ram_on = round(sum_ram_on)
            self.fd.write("<tr class=\"trsum\">")
            self.fd.write("<td></td>")
            self.fd.write("<td></td>")
            self.fd.write("<td>%s GB ON / %s GB OFF</td>" %
                          (sum_ram_on, sum_ram_off))
            self.fd.write("<td>%s ON/ %s OFF</td>" % (sum_cpu_on, sum_cpu_off))
            self.fd.write("<td></td>")
            self.fd.write("<td></td>")
            self.fd.write("<td></td>")
            self.fd.write("</tr>")
            self.fd.write("</table></div>")

        self.fd.write("</div></div>")
        self.fd.flush()

    def save_html_vnics(self, vnics):
        self.fd.write("""
        <div class="sectitle">
            <div>VMware VLANs and portgroups</div> 
            <div>
        """)

        for vcenter in vnics:
            self.fd.write("""
            <div class="subsectitle">vCenter: %s</div>
            <div class="data">
                <table>
                    <tr class="dataheader">
                        <th>portgroup</th>
                        <th>VLAN</th>
                    </tr>
            """ % (vcenter))
            for v in vnics[vcenter]:
                self.fd.write("<tr class=\"datatr\">")
                self.fd.write("<td>%s</td>" % v)
                self.fd.write("<td>%s</td>" % vnics[vcenter][v])
                self.fd.write("</tr>")
            self.fd.write("</table></div>")

        self.fd.write("</div></div>")
        self.fd.flush()

    def save_html_dts(self, dts):
        self.fd.write("""
        <div class="sectitle">
            <div>VMware datastores</div> 
            <div>
        """)

        for vcenter in dts:
            self.fd.write("""
            <div class="subsectitle">vCenter: %s</div>
            <div class="data">
                <table>
                    <tr class="dataheader">
                        <th>datastore</th>
                        <th>capacity GB</th>
                        <th>free GB</th>
                        <th>used GB</th>
                    </tr>
            """ % (vcenter))
            for d in dts[vcenter]:
                self.fd.write("<tr class=\"datatr\">")
                self.fd.write("<td>%s</td>" % d['name'])
                self.fd.write("<td>%s</td>" % d['capacity'])
                self.fd.write("<td>%s</td>" % d['free'])
                self.fd.write("<td>%s</td>" %
                              (float(d['capacity']) - float(d['free'])))
                self.fd.write("</tr>")
            self.fd.write("</table></div>")

        self.fd.write("</div></div>")
        self.fd.flush()

    def save_html_lun(self, luns):
        self.fd.write("""
        <div class="sectitle">
            <div>non-VMware LUN-ovi, npr. LPAR, dedicated host</div> 
            <div>
        """)

        for storage in luns:
            for sn in luns[storage]:
                self.fd.write("""
                <div class="subsectitle">%s SN: %s</div>
                <div class="data">
                    <table>
                        <tr class="dataheader">
                            <th>LUN</th>
                            <th>Label</th>
                            <th>size GB</th>
                            <th>consumed GB</th>
                            <th>pool ID</th>
                            <th>tier0 GB</th>
                            <th>tier1 GB</th>
                            <th>tier2 GB</th>
                        </tr>
                """ % (storage, sn))
                for l in luns[storage][sn]:
                    self.fd.write("<tr class=\"datatr\">")
                    self.fd.write("<td>%s</td>" % l['displayName'])
                    self.fd.write("<td>%s</td>" % l['label'])
                    self.fd.write("<td>%s</td>" % str(l['size']))
                    self.fd.write("<td>%s</td>" % l['consumed'])
                    self.fd.write("<td>%s</td>" % l['dpPoolID'])
                    self.fd.write("<td>%s</td>" % l['t0consumed'])
                    self.fd.write("<td>%s</td>" % l['t1consumed'])
                    self.fd.write("<td>%s</td>" % l['t2consumed'])
                    self.fd.write("</tr>")
                self.fd.write("</table></div>")

        self.fd.write("</div></div>")
        self.fd.flush()

    def save_html_lpars(self, lpars):
        self.fd.write("""
        <div class="sectitle">
            <div>LPAR-i</div> 
            <div>
        """)

        self.fd.write("""
        <div class="data">
            <table>
                <tr class="dataheader">
                    <th>ID</th>
                    <th>Naziv</th>
                    <th>Status</th>
                    <th>RAM GB</th>
                    <th>Dedicated CPU</th>
                    <th>Shared CPU</th>
                    <th># FC ports</th>
                    <th># NIC ports</th>
                    <th>VLANs</th>
                </tr>
        """)

        sum_dedcpu_act = 0
        sum_dedcpu_deact = 0
        sum_shrcpu_act = 0
        sum_shrcpu_deact = 0
        sum_ram_act = 0
        sum_ram_deact = 0
        for l in lpars:
            power_class = ''
            if l['status'] == 'DEACT':
                power_class = 'off'
                sum_dedcpu_deact += int(l['dedcpu'])
                sum_shrcpu_deact += int(l['shrcpu'])
                sum_ram_deact += int(l['ramgb'])
            else:
                sum_dedcpu_act += int(l['dedcpu'])
                sum_shrcpu_act += int(l['shrcpu'])
                sum_ram_act += int(l['ramgb'])
            vlans = ",".join(l['vlans'])

            self.fd.write("<tr class=\"datatr%s\">" % power_class)
            self.fd.write("<td>%s</td>" % l['id'])
            self.fd.write("<td>%s</td>" % l['naziv'])
            self.fd.write("<td>%s</td>" % l['status'])
            self.fd.write("<td>%s</td>" % l['ramgb'])
            self.fd.write("<td>%s</td>" % l['dedcpu'])
            self.fd.write("<td>%s</td>" % l['shrcpu'])
            self.fd.write("<td>%s</td>" % l['fcports'])
            self.fd.write("<td>%s</td>" % l['nics'])
            self.fd.write("<td>%s</td>" % vlans)
            self.fd.write("</tr>")
        self.fd.write("<tr class=\"trsum\">")
        self.fd.write("<td></td>")
        self.fd.write("<td></td>")
        self.fd.write("<td></td>")
        self.fd.write("<td>%s GB ON / %s GB OFF</td>" %
                      (sum_ram_act, sum_ram_deact))
        self.fd.write("<td>%s ON/ %s OFF</td>" %
                      (sum_dedcpu_act, sum_dedcpu_deact))
        self.fd.write("<td>%s ON/ %s OFF</td>" %
                      (sum_shrcpu_act, sum_shrcpu_deact))
        self.fd.write("<td></td>")
        self.fd.write("<td></td>")
        self.fd.write("<td></td>")
        self.fd.write("</tr>")
        self.fd.write("</table></div>")

        self.fd.write("</div></div>")
        self.fd.flush()

    def save_header(self, datetime):
        self.fd.write("""
        <html>
            <head>
                <meta charset="UTF-8">
                <title>usage {}</title>
                <link rel="stylesheet" href="korisnici.css">
            </head>
        <body class="maindata">
        """.format(datetime))
        self.fd.flush()

    def save_footer(self):
        self.fd.write("""
        </body>
        </html>""")
        self.fd.flush()

    def save_kk(self, kkid):
        if kkid not in config.cfg.korisnici:
            print("ERROR missing config KK ID:", kkid)
            config.cfg.korisnici[kkid] = "*MISSING NAME!!! FIX CONFIG*"

        self.fd.write("""
        <div id="%s" class="kk">
            %s: %s 
        </div>
        """ % (kkid, kkid,
               config.cfg.korisnici[kkid]
               ))

    def save_html(self, datetime):
        self.fd = open(self.filename, "w")

        self.save_header(datetime)

        for kk in self.kks:
            self.save_kk(kk)

            if 'vms' in self.kks[kk]:
                self.save_html_vms(self.kks[kk]['vms'])
            if 'vnics' in self.kks[kk]:
                self.save_html_vnics(self.kks[kk]['vnics'])
            if 'dts' in self.kks[kk]:
                self.save_html_dts(self.kks[kk]['dts'])
            if 'lun' in self.kks[kk]:
                self.save_html_lun(self.kks[kk]['lun'])
            if 'lpar' in self.kks[kk]:
                self.save_html_lpars(self.kks[kk]['lpar'])

        self.save_footer()
        self.fd.close()

    def get_ram_cpu(self, vms):
        """ sum_ram_on, sum_ram_off, sum_cpu_on, sum_cpu_off, vms_on, vms_off """

        sum_cpu_on = 0
        sum_cpu_off = 0
        sum_ram_on = 0
        sum_ram_off = 0
        vms_on = 0
        vms_off = 0
        for vcenter in vms:
            for v in vms[vcenter]:
                if v['power-state'] == 'poweredOff':
                    vms_off += 1
                    if v['cpu']:
                        sum_cpu_off += int(v['cpu'])
                    if v['ram-mb']:
                        sum_ram_off += (int(v['ram-mb'])/1024)
                else:
                    sum_cpu_on += int(v['cpu'])
                    sum_ram_on += (int(v['ram-mb']) / 1024)
                    vms_on += 1
            sum_ram_off = round(sum_ram_off)
            sum_ram_on = round(sum_ram_on)
        return (sum_ram_on, sum_ram_off, sum_cpu_on, sum_cpu_off, vms_on, vms_off)

    def get_dts_capacity(self, dts):
        sum = 0
        sum_used = 0
        uniqdts = {}
        for vcenter in dts:
            for d in dts[vcenter]:
                if d['name'] in uniqdts and d['capacity'] == uniqdts[d['name']]:
                    print("skip duplicate datastore %s, %s GB" %
                          (d['name'], d['capacity']))
                    continue
                uniqdts[d['name']] = d['capacity']
                sum += int(d['capacity'])
                sum_used += int(float(d['capacity']) - float(d['free']))

        return sum, sum_used

    def get_lun_capacity(self, luns):
        sum = 0
        sum_used = 0
        for storage in luns:
            for sn in luns[storage]:
                for l in luns[storage][sn]:
                    sum += int(l['size'])
                    if l['consumed']:
                        sum_used += int(l['consumed'])
                    else:
                        sum_used += int(l['size'])

        return sum, sum_used

    def get_lpar_capacity(self, lpars):
        sum_dedcpu_act = 0
        sum_dedcpu_deact = 0
        sum_shrcpu_act = 0
        sum_shrcpu_deact = 0
        sum_ram_act = 0
        sum_ram_deact = 0

        for l in lpars:
            if l['status'] == 'DEACT':
                sum_dedcpu_deact += int(l['dedcpu'])
                sum_shrcpu_deact += int(l['shrcpu'])
                sum_ram_deact += int(l['ramgb'])
            else:
                sum_dedcpu_act += int(l['dedcpu'])
                sum_shrcpu_act += int(l['shrcpu'])
                sum_ram_act += int(l['ramgb'])

        return sum_dedcpu_deact, sum_dedcpu_act, sum_shrcpu_act, sum_shrcpu_deact, sum_ram_deact, sum_ram_act

    def save_xls(self, xls):

        for kk in self.kks:
            if kk not in config.cfg.korisnici:
                print("ERROR missing config KK ID:", kk)
                config.cfg.korisnici[kk] = "*MISSING NAME!!! FIX CONFIG*"

            cpu_on = 0
            cpu_off = 0
            ram_on = 0
            ram_off = 0
            disk = 0
            disk_used = 0
            sum_dedcpu_deact = 0
            sum_dedcpu_act = 0
            sum_shrcpu_act = 0
            sum_shrcpu_deact = 0
            sum_ram_deact = 0
            sum_ram_act = 0
            vms_on = 0
            vms_off = 0

            if 'vms' in self.kks[kk]:
                (ram_on, ram_off, cpu_on, cpu_off, vms_on,
                 vms_off) = self.get_ram_cpu(self.kks[kk]['vms'])
            if 'dts' in self.kks[kk]:
                (disk, disk_used) = self.get_dts_capacity(self.kks[kk]['dts'])
            if 'lun' in self.kks[kk]:
                (t1, t2) = self.get_lun_capacity(self.kks[kk]['lun'])
                disk += t1
                disk_used += t2
            if 'lpar' in self.kks[kk]:
                (sum_dedcpu_deact, sum_dedcpu_act, sum_shrcpu_act, sum_shrcpu_deact, sum_ram_deact, sum_ram_act) = \
                    self.get_lpar_capacity(self.kks[kk]['lpar'])

            xls.write_user(kk, config.cfg.korisnici[kk],
                           ram_on, ram_off, cpu_on, cpu_off,
                           disk, disk_used,
                           sum_dedcpu_deact, sum_dedcpu_act, sum_shrcpu_act, sum_shrcpu_deact, sum_ram_deact,
                           sum_ram_act, vms_on, vms_off
                           )

    def gen_xlsx_index(self):
        files = []

        for fname in os.listdir("."):
            if fname.startswith('capacity') and fname.endswith('.xlsx'):
                statinfo = os.stat(fname)
                files.append((fname, statinfo.st_mtime))

        fd = open('capacity.html', "w")
        fd.write("""
        <html>
            <head>
                <meta charset="UTF-8">
                <title>capacity report</title>
                <link rel="stylesheet" href="korisnici.css">
            </head>
            <body class="maindata">
                <div>
                    <ul>
        """)

        for f in sorted(files, key=itemgetter(1), reverse=True):
            fd.write("<li><a href=\"{0}\">{0}</a></li>\n".format(f[0]))

        fd.write("""
                    </ul>
                </div>
            </body>
        </html>""")

        fd.close()
