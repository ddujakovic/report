import xml.etree.ElementTree as etree
import subprocess
import re
import io

###reporter ssh key na hcs.itsoft.loc - R1...!
###user se spaja i pokreće bash skriptu za skupljanje podataka storage sustava na hcs-u koristeći iaasreporter (kreiran an hcs-u)

class IaaSReportHCS(object):
    "IaaSReportHCS"
#d+ one or more digits
#npr. fw001.setcor.com
#ovo se ignorira i zapisuje sve lunove u tablicu zbog lakšeg generiranaj reporta - notour = True
    def __init__(self):
        self.ouresx = re.compile(r'(vspp\d+\.itsoft\.loc|'
                                 r'vsppzg\d+\.itsoft\.loc|'
                                 r'esx\d+\.itsoft\.loc|'
                                 r'esxi\d+\.setcor\.com|'
                                 r'fw\d+\.setcor\.com|'
                                 r'v\d+\.setcor\.com|'
                                 r'z\d+\.setcor\.com|'
                                 r'sapesx\d+\.setcor.com|'
                                 r'oracle\.setcor\.com|'
                                 r'fw.geant\.setcor\.com|'
                                 r'D+\d+\.itsoft\.loc|'
                                 r'D+\d+\.setcor\.com)')
        self.wwnhost = {}

    def getarrays(self):
        " getarrays "
        xml = subprocess.check_output(["ssh reporter@hcs.itsoft.loc -t -t \"sudo /opt/getarrays.sh\""], 
                                    shell=True)
        root = etree.fromstring(xml)

        out = []
        for result in root:
            for storagearray in result:
                array = {
                    'arrayType': storagearray.attrib['arrayType'],
                    'serialNumber': storagearray.attrib['serialNumber'],
                    'displayArrayType': storagearray.attrib['displayArrayType']
                }
                out.append(array)
        return out

    def getluns(self, model, sn):
        """
        <LogicalUnit
            <Path
                <HostInfo
                <WWN
            <LDEV
                <ObjectLabel
        """

        xml = subprocess.check_output(["ssh reporter@hcs.itsoft.loc -t -t \"sudo /opt/getluns.sh %s %s\""
                                       % (model, sn)],
                                      shell=True)
        xml = xml.decode('utf-8')
        xml = re.sub(r'</VolumeC', '></VolumeC', xml)
        root = etree.fromstring(xml)

        out = []
        for result in root:
            for storages in result:
                for logun in storages:
                    notour = True
                    lundata = {}
                    for ldev in logun:
                        if ldev.tag == 'LDEV':
                            lundata['displayName'] = ldev.attrib['displayName']
                            lundata['consumed'] = float(
                                "%.2f" % (int(ldev.attrib['consumedSizeInKB'])/1024/1024))

                            tierconsumed = ldev.attrib['dpTier0ConsumedCapacityInKB']
                            if tierconsumed == '-1':
                                tierconsumed = 0
                            else:
                                tierconsumed = float(
                                    "%.2f" % (int(ldev.attrib['dpTier0ConsumedCapacityInKB'])/1024/1024))
                            lundata['t0consumed'] = tierconsumed

                            tierconsumed = ldev.attrib['dpTier1ConsumedCapacityInKB']
                            if tierconsumed == '-1':
                                tierconsumed = 0
                            else:
                                tierconsumed = float(
                                    "%.2f" % (int(ldev.attrib['dpTier1ConsumedCapacityInKB'])/1024/1024))
                            lundata['t1consumed'] = tierconsumed

                            tierconsumed = ldev.attrib['dpTier2ConsumedCapacityInKB']
                            if tierconsumed == '-1':
                                tierconsumed = 0
                            else:
                                tierconsumed = float(
                                    "%.2f" % (int(ldev.attrib['dpTier2ConsumedCapacityInKB'])/1024/1024))
                            lundata['t2consumed'] = tierconsumed

                            lundata['size'] = float(
                                "%.2f" % (int(ldev.attrib['sizeInKB'])/1024/1024))
                            lundata['dpPoolID'] = ldev.attrib['dpPoolID']
                            for ldevsubobj in ldev:
                                if ldevsubobj.tag == "ObjectLabel":
                                    lundata['label'] = ldevsubobj.attrib['label']
                            if 'label' not in lundata:
                                lundata['label'] = 'MISSING LABEL !!!!'
                        elif ldev.tag == 'Path':
                            for host in ldev:
                                wwnkey = 'WWN'
                                if 'portWWN' in host.attrib:
                                    wwnkey = 'portWWN'
                                if wwnkey in host.attrib:
                                    if host.attrib[wwnkey] in self.wwnhost:
                                        if not self.ouresx.match(self.wwnhost[host.attrib[wwnkey]]['name']):
                                            notour = True
                    if notour:
                        out.append(lundata)
                        lundata = {}
                        notour = True
        out = sorted(out, key=lambda k: k['label'].lower())
        return out

    def gethosts(self):
        " get hosts "
        hostsin = subprocess.check_output(["ssh reporter@hcs.itsoft.loc -t -t \"sudo /opt/gethost.sh\""],
                                          shell=True)

        self.wwnhost = {}
        host = {}
        buff = io.StringIO(hostsin.decode('utf-8'))
        for line in buff:
            if line[0:3] == 'An ':
                host = {}
            elif line[0:6] == '  name':
                host['name'] = line[7:len(line)-2]
            elif line[0:8] == '  osType':
                host['osType'] = line[9:len(line)-2]
            elif line[0:9] == '      WWN':
                if 'osType' not in host:
                    host['osType'] = ''
                self.wwnhost[line[10:len(line)-2]] = host
