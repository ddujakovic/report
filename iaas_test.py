import mysql.connector
from mysql.connector import Error
from datetime import datetime
import config
import iaasreportvmw
import iaasreporthcs
import iaasreportops
import veeamvsppreport
import re
import commvault
import snapshots

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )

#mycursor = mydb.cursor()
time=datetime.now()
time=time.strftime("%d.%m.%Y %H:%M")
re_kid = re.compile(r'.*(K\d\d\d\d)')

def prepare_tables(mycursor):
    ##################
    ###dnevnireport###
    ##################
    mycursor.execute("CREATE TABLE IF NOT EXISTS dnevnireport (id INT AUTO_INCREMENT PRIMARY KEY, created VARCHAR(255))")
    mycursor.execute("SELECT MAX(id) FROM dnevnireport")
    id=mycursor.fetchone()[0]
    if id:
        id += 1
    else:
        id = 1
    
    insertdata= """INSERT INTO dnevnireport (id, created) VALUES ( %s, NOW())"""
    data=(id,)
    mycursor.execute(insertdata, data)
    return id

def do_snapshots(mycursor,suffix):
    for addr, desc in config.cfg.vcenters.items():
        print(desc)
        snaps = snapshots.snapshotclass(addr,config.cfg.USERNAME,config.cfg.PASSWORD,443)
        snapshot = snaps.get_snapshots()

        ### VMWARE RESURSI vcpu,ram,...
        sql = """INSERT INTO vmwaresnapshots (cid, kid, uuid, vm, rp, folder, snapshotDate, snapshotName) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

        for item in snapshot:
            # item['folder'] za suffix ima ID korisnika, Kxxxx
            # šifra u folderu umjesto resource poolu
            m = re_kid.match(item['folder'])
            kid = None
            if m:
                kid= m.group(1)
            else:
                m = re_kid.match(item['rp'])
                if m:
                    kid = m.group(1)

            mycursor.execute(sql, (suffix, kid, item['uuid'], item['vm'], item['rp'], item['folder'], item['snapshotDate'], item['snapshotName']))

        snaps.disconnect()



def do_veeamvspp(mycursor,suffix):
    veeam = veeamvsppreport.veeamvspp()
    veeamjobs = veeam.getveeamvsppjobs()
    veeambackupfiles = veeam.getveeamvsppbackupfiles()
    veeambackupfiledetails = veeam.getbackupfiledetails(veeambackupfiles)
    veeambackupid= veeam.getveeambackupid()
    veeambackupvmcount = veeam.getveeambackupvmcount(veeambackupid)
    #print(veeambackupfiledetails)
    sql = """INSERT INTO veeambackupjobs (cid, kid, jobname) VALUES (%s, %s, %s)"""
    for item in veeamjobs:
        print("Backup Job: ", item.get("JobName"))
        m = re_kid.match(item['JobName'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['JobName']))

    sql = """INSERT INTO veeambckfiles (cid, bckfilehref) VALUES (%s, %s)"""
    for item in veeambackupfiles:
        mycursor.execute(sql, (suffix, item['BackupFile']))
    
    sql = """INSERT INTO veeambckdetail (cid, kid, bckfilename, bckfilesize, bckfilepath) VALUES (%s, %s, %s, %s, %s)"""
    for item in veeambackupfiledetails:
        m = re_kid.match(item['FilePath'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['Name'], item['BackupSize'], item['FilePath']))
    
    sql = """INSERT INTO veeamvmcount (cid, kid, vmcount) VALUES (%s, %s, %s)"""
    for item in veeambackupvmcount:
        #print(item['JobName'])
        m = re_kid.match(item['JobName'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['VMCount']))
        

def do_commvault(mycursor,suffix):
    comm=commvault.commv()
    #client name, id, group, K$$$$
    #commvault_clients=comm.client_id()
    #agent name, os, group, license
    commvault_client_details=comm.clientdetails()
    #sql = """INSERT INTO commvaultkorisnici (cid, kid, commname, commgroupid, commgroup) values (%s, %s, %s, %s, %s)"""
    #for item in commvault_clients:
    #    m = re_kid.match(item['Description'])
    #    kid = None
    #    if m:
    #        kid = m.group(1)
    #    mycursor.execute(sql, (suffix, kid, item['ClientName'], item['ClientID'], item['CommvaultGroup']))
    #
    #sql = """INSERT INTO commvaultagenti (cid, clientdisplayname, clientos, commgroupid, clientlicense) values (%s, %s, %s, %s, %s)"""
    #for item in commvault_client_details:
    #    mycursor.execute(sql, (suffix, item['clientDisplayName'], item['clientOSName'], item['clientCommvaultGroupId'], item['clientLicenses']))
    #commvault_libraries=comm.commvaultlibraries()
    #commvault_libusage=comm.commvaultlibraryusage(commvault_libraries)
    #sql = """INSERT INTO commvaultlibraryusage (cid, libname, libid, libtotal, libavailable) values (%s, %s, %s, %s, %s)"""
    #for item in commvault_libusage:
    #    mycursor.execute(sql, (suffix, item['LibraryName'], item['LibraryID'], item['LibraryTotal'], item['LibraryAvailable']))

def do_hitachi_ops(mycursor,suffix):
    ops = iaasreportops.iaasreportOPS()
    storages = ops.getstorages()
    #print("IAAS_TEST")
    #print(storages)
    #print("IAAS_TEST")
    for store in storages:
        #print("Storage system: ", store.get("storageSystemName"))
        #print("Storage system: ", store.get("storageSystemId"))
        storage = store.get("storageSystemId")
        #print(storage)
        luns = ops.getluns(storage)

        sql = """INSERT INTO storages (cid, storagename, storageid, ldev, kid, volname, capacitygb, consumedgb) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""
        for item in luns:
            #print(luns)
            m = re_kid.match(item['displayName'])
            kid = None
            if m:
                kid = m.group(1)
                #print(store['storageSystemName'])
                #print(store['storageSystemId'])
                #print(item['displayName'])
                #print(kid)
                #print(item['size'])
                #print(item['consumed'])
                #print(item['tiername'])
            mycursor.execute(sql, (suffix, store['storageSystemName'], store['storageSystemId'], item['ldev'], kid, item['displayName'], item['size'], item['consumed']))
   
def do(mycursor):
    suffix = prepare_tables(mycursor)
    #do_korisnici()
    #do_veeamvspp(mycursor,suffix)
    #do_commvault(mycursor,suffix)
    #do_snapshots(mycursor,suffix)
    do_hitachi_ops(mycursor,suffix)

if __name__ == "__main__":
    mycursor = mydb.cursor()
    do(mycursor)
    mydb.commit()
    mydb.close()
