#IAASREPORTOPS 04/2023
import os
import subprocess
import json
import requests
import http.client
import urllib3
import config
from jsonpath_ng import jsonpath, parse

urllib3.disable_warnings()

# create a session object and set headers
session = requests.Session()
session.headers.update({"Authorization": config.cfg.hopsadm}) 
session.verify = False# disable SSL verification for the session
session.max_age = 30  # expire after 30 seconds

# make a POST request to get a token
url = "https://hopsadm.setcor.com/v1/security/tokens"
response = session.post(url)

# extract the token ID from the response headers
tokenID = response.headers.get("X-Auth-Token")

class iaasreportOPS(object):
##############
###STORAGES###
############## 
    """
    iz iaas.py pokreće 
    def do_hitachi_ops(mycursor,suffix)
    ops = iaasreportops.iaasreportOPS()
    storages = ops.getstorages() # pokreće funkciju ovdje i vraća sav storage iz hopsadm
    for petlja za svaki storage
    storage = store.get("storageSystemId") # npr 452734
    luns = ops.getluns(storage) # pokreće funkciju ovdje i vraća sve lunove"""
    def getstorages(self):
        url = "https://hopsadm.setcor.com/v1/storage-systems/"
        headers = {
            'X-Auth-Token': str(tokenID),
            'Content-Type': "application/json"
        }

        response = requests.get(url, headers=headers, verify=False)
        response.raise_for_status()

        json_data = response.json()
        storagesout = [
            {
                'storageSystemName': str(match['storageSystemName']),
                'storageSystemId': str(match['storageSystemId'])
            }
            for match in json_data['resources']
        ]

        # print(storagesout)
        return storagesout
############
###LUNOVI###
############
    def getluns(self,storagesout):
        hostlunsout = []
        storageports = []
        url = f"https://hopsadm.setcor.com/v1/storage-systems/{storagesout}/storage-ports"
        headers = {
            'X-Auth-Token': str(tokenID),
            'Content-Type': "application/json"
            }
        response = requests.request("GET", url, headers=headers, verify=False)
        storageports = response.json()
        jsonpath_expression = parse('$.resources[*].storagePortId')
        chb = [match.value for match in jsonpath_expression.find(storageports)]
        lst= list(range(0,131))#max 100 hostova po portu, povecati broj ukoliko npr. na G800 ima više od CL1-A-130 https://$hitachiopsadministrator/ui/#/storage-systems/470731/host-groups
        for index_chb in chb:
            for index_lst in lst:
                #print(index_chb)
                #print(index_lst)
                url = f"https://hopsadm.setcor.com/v1/storage-systems/{storagesout}/host-groups/{index_chb}-{index_lst}"                    
                headers = {
                    'X-Auth-Token': str(tokenID),
                    'Content-Type': "application/json"
                }
                try:
                    response = requests.request("GET", url, headers=headers, verify=False)
                    response.raise_for_status() # raises exception when not a 2xx response
                    hostluns = response.json()
                    jsonpath_expression = parse('$.luns[*].volumeId')
                    volume_ids = [match.value for match in jsonpath_expression.find(hostluns)]
                    volume_ids.sort()
                    #print(volume_ids)
                    for unique in volume_ids:
                        if unique not in hostlunsout:
                            hostlunsout.append(unique)
                except:
                    pass
        hostlunsout.sort()
        print(hostlunsout)
        print(len(hostlunsout))
        ####
        lunsout = []

        for VolId in range(len(hostlunsout)):
            url = f"https://hopsadm.setcor.com/v1/storage-systems/{storagesout}/volumes/{hostlunsout[VolId]}"
            headers = {
                'X-Auth-Token': str(tokenID),
                'Content-Type': "application/json"
            }
            response = requests.get(url, headers=headers, verify=False)
            response.raise_for_status()

            luns = response.json()
            #jsonpath_expression = parse('resources[*]')
            #for match in luns:
            lundata = {
                'displayName': str(luns['label']),
                'size': round(float(luns['size']) / 1024 / 1024 / 1024, 2),
                'ldev': str(luns['volumeId']),
                'consumed': round(float(luns['usedCapacity']) / 1024 / 1024 / 1024, 2),
                'luntier': str(luns['tieringPolicy'])
            }
            lunsout.append(lundata)
        print(lunsout)

        return lunsout