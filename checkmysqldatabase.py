import requests
import sys
import xml.etree.ElementTree as ET
import base64
import json
import mysql.connector
from datetime import datetime, date, timedelta
import config
import re
import urllib3
#email
import smtplib
from email.mime.text import MIMEText
###Disable HTTPS warnings https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )
date_format= "%Y-%m-%d"
datum=datetime.now()
#datum=datum.strftime("%Y-%m-%d")
print(datum)
print("START CHECK...")
def checkssl(mycursor):
    #mycursor = mysql.connection.cursor()
    mycursor.execute("SELECT sslid, sslend, sslnaziv, sslopis FROM sslcert;")
    sslcerts=mycursor.fetchall()
    for index in range(len(sslcerts)):
        #print(sslcerts[index][1])
        sslend=str(sslcerts[index][1])
        sslid=str(sslcerts[index][0])
        sslend=datetime.strptime(sslend, "%Y-%m-%d")
        #sslend=datetime.strftime(sslend, "%Y-%m-%d")
        #print(sslend)
        delta = sslend.date() - datum.date()
        delta = delta.days
        #print(delta)
        #print(delta.days)
        if delta > 14:
            mycursor.execute("UPDATE sslcert SET isexpired='False' where sslid like %s;", (sslid, ))
        if 0 < delta < 14:
            mycursor.execute("UPDATE sslcert SET isexpired='Soon' where sslid like %s;", (sslid, ))
        if delta < 7:
            mycursor.execute("SELECT sslmail FROM sslcert where sslid like %s;", (sslid, ))
            receiver=mycursor.fetchall()
            mycursor.execute("SELECT sslnaziv FROM sslcert where sslid like %s;", (sslid, ))
            sslnaziv=mycursor.fetchall()
            mycursor.execute("SELECT sslfirma FROM sslcert where sslid like %s;", (sslid, ))
            sslfirma=mycursor.fetchall()
            mycursor.execute("SELECT sslopis FROM sslcert where sslid like %s;", (sslid, ))
            sslopis=mycursor.fetchall()
            mycursor.execute("SELECT sslend FROM sslcert where sslid like %s;", (sslid, ))
            sslend=mycursor.fetchall()
            #print(receiver)
            sendmailssl(receiver,sslnaziv,sslfirma,sslopis,sslend,delta)
        if delta <= 0:
            mycursor.execute("UPDATE sslcert SET isexpired='True' where sslid like %s;", (sslid, ))

def sendmailssl(receiver,sslnaziv,sslfirma,sslopis,sslend,delta):
    sender = 'reporter@setcor.com'
    receivers = receiver
    primatelj = str(receivers)[3:-4]
    sslnaziv=str(sslnaziv)[3:-4]
    sslfirma=str(sslfirma)[3:-4]
    sslopis=str(sslopis)[3:-4]
    sslend=str(sslend)[3:-4]

    delta = delta
    if delta == 1:
        subjekt="SSL reminder - "+sslnaziv+"expires in %s"%(delta)+" day!"
        msg0 = ("SSL certificate expires in %s day" %(delta) +"\n")
    elif delta == 0:  
        subjekt="SSL reminder - "+sslnaziv+"expires today!"
        msg0 = ("SSL certificate expires today!\n")
    elif delta == -1:
        subjekt="SSL reminder - "+sslnaziv+"expired %s"%(delta)+" day ago!"
        msg0 = ("SSL certificate expired %s day ago" %(delta) +"\n")
    elif delta < -1:
        subjekt="SSL reminder - "+sslnaziv+"expired %s"%(delta)+" days ago!"
        msg0 = ("SSL certificate expired %s days ago" %(delta) +"\n")
    else:
        subjekt="SSL reminder - "+sslnaziv+"expires in %s"%(delta)+" days!"
        msg0 = ("SSL certificate expires in %s days" %(delta) +"\n")

    msg1 = ("SSL naziv - " + sslnaziv +"\n")
    msg2 = ("SSL firma - " + sslfirma +"\n")
    msg3 = ("SSL opis - " + sslopis +"\n")
    msg4 = ("SSL expire  date - " + sslend)
    msg=msg0+msg1+msg2+msg3+msg4
    msg = MIMEText(msg)
    subjekt="SSL reminder - "+sslnaziv+"expires in %s"%(delta)+" days!"
    msg['Subject'] = subjekt
    msg['From'] = 'sslreminder@setcor.com'
    msg['To'] = 'noreply@setcor.com'

    with smtplib.SMTP('localhost') as server:
        
        # server.login('username', 'password')
        server.sendmail(sender, receivers, msg.as_string())
        print("Successfully sent email for SSL '"+sslnaziv+"' to "+primatelj)

def checklic(mycursor):
    #mycursor = mysql.connection.cursor()
    mycursor.execute("SELECT licid, licend, licnaziv, licopis FROM licence;")
    licences=mycursor.fetchall()
    for index in range(len(licences)):
        #print(licences[index][1])
        licend=str(licences[index][1])
        licid=str(licences[index][0])
        licend=datetime.strptime(licend, "%Y-%m-%d")
        #licend=datetime.strftime(licend, "%Y-%m-%d")
        #print(licend)
        delta = licend.date() - datum.date()
        delta = delta.days
        #print(delta)
        #print(delta.days)
        if delta > 14:
            mycursor.execute("UPDATE licence SET isexpired='False' where licid like %s;", (licid, ))
        if 0 < delta < 14:
            mycursor.execute("UPDATE licence SET isexpired='Soon' where licid like %s;", (licid, ))
        if delta < 7:
            mycursor.execute("SELECT licmail FROM licence where licid like %s;", (licid, ))
            receiver=mycursor.fetchall()
            mycursor.execute("SELECT licnaziv FROM licence where licid like %s;", (licid, ))
            licnaziv=mycursor.fetchall()
            mycursor.execute("SELECT licfirma FROM licence where licid like %s;", (licid, ))
            licfirma=mycursor.fetchall()
            mycursor.execute("SELECT licopis FROM licence where licid like %s;", (licid, ))
            licopis=mycursor.fetchall()
            mycursor.execute("SELECT licend FROM licence where licid like %s;", (licid, ))
            licend=mycursor.fetchall()
            #print(receiver)
            sendmaillic(receiver,licnaziv,licfirma,licopis,licend,delta)
        if delta <= 0:
            mycursor.execute("UPDATE licence SET isexpired='True' where licid like %s;", (licid, ))

def sendmaillic(receiver,licnaziv,licfirma,licopis,licend,delta):
    sender = 'reporter@setcor.com'
    receivers = receiver
    primatelj = str(receivers)[3:-4]
    licnaziv=str(licnaziv)[3:-4]
    licfirma=str(licfirma)[3:-4]
    licopis=str(licopis)[3:-4]
    licend=str(licend)[3:-4]

    delta = delta
    if delta == 1:
        subjekt="License reminder - "+licnaziv+"expires in %s"%(delta)+" day!"
        msg0 = ("License expires in %s day" %(delta) +"\n")
    elif delta == 0:  
        subjekt="License reminder - "+licnaziv+"expires today!"
        msg0 = ("License expires today!\n")
    elif delta == -1:
        subjekt="License reminder - "+licnaziv+"expired %s"%(delta)+" day ago!"
        msg0 = ("License expired %s day ago" %(delta) +"\n")
    elif delta < -1:
        subjekt="License reminder - "+licnaziv+"expired %s"%(delta)+" days ago!"
        msg0 = ("License expired %s days ago" %(delta) +"\n")
    else:
        subjekt="License reminder - "+licnaziv+"expires in %s"%(delta)+" days!"
        msg0 = ("License expires in %s days" %(delta) +"\n")
    
    msg1 = ("lic naziv - " + licnaziv +"\n")
    msg2 = ("lic firma - " + licfirma +"\n")
    msg3 = ("lic opis - " + licopis +"\n")
    msg4 = ("lic expire date - " + licend)
    msg=msg0+msg1+msg2+msg3+msg4
    msg = MIMEText(msg)
    msg['Subject'] = subjekt
    msg['From'] = 'licreminder@setcor.com'
    msg['To'] = 'noreply@setcor.com'

    with smtplib.SMTP('localhost') as server:
        
        # server.login('username', 'password')
        server.sendmail(sender, receivers, msg.as_string())
        print("Successfully sent email for license '"+licnaziv+"' to "+primatelj)

def do(mycursor):
    checkssl(mycursor)
    checklic(mycursor)
    print("CHECK END...")
    
if __name__ == "__main__":
    mycursor = mydb.cursor()
    do(mycursor)
    mydb.commit()
    mydb.close()
