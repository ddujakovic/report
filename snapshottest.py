import pprint
import ssl
import itertools
import operator
import iaasreportvmw
import config

from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim

for addr, desc in config.cfg.vcenters.items():
    print(desc)
    snaps = []
    try:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        ssl_context.verify_mode = ssl.CERT_NONE
        vcenter_connect = connect.SmartConnect(host=addr, user=config.cfg.USERNAME, pwd=config.cfg.PASSWORD, port=int(443), sslContext=ssl_context)
        print(vcenter_connect.CurrentTime())
        #get_snapshots(vcenter_connect)

        content = vcenter_connect.RetrieveContent()

        container = content.rootFolder  # starting point to look into
        viewType = [vim.VirtualMachine]  # object types to look for
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
                container, viewType, recursive)

        children = containerView.view

        #print(children)
        #print(children[0].snapshot)

        #snaps = []
        for virtual_machine in children:
            summary = virtual_machine.summary
            snapshot = virtual_machine.snapshot
            #print(summary)
            if hasattr(virtual_machine.resourcePool, 'name'):
                rpool = virtual_machine.resourcePool.name
            else:
                rpool = "root"
            if hasattr(virtual_machine.snapshot, 'currentSnapshot'):
                snaps.append({
                    'uuid': summary.config.instanceUuid,
                    'vm': summary.config.name,
                    'pool': rpool,
                    'snapshotDate': snapshot.currentSnapshot.createTime,
                    'snapshotName': snapshot.currentSnapshot.name
                })
                print(snaps)
        #print(snaps)


        connect.Disconnect(vcenter_connect)
    except:
        print("ERROR vmware")
