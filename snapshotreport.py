#Generira dnevni snapshot report i salje na mail
import mysql.connector
from mysql.connector import Error
import xlsxwriter
import config
from datetime import datetime

time=datetime.now()
time=time.strftime("%d.%m.%Y")

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )

def snapshot_report(mycursor):
        mycursor.execute("SELECT MAX(id) FROM dnevnireport")
        lastconfig=mycursor.fetchone()[0]
        #create workbook and sheet
        workbook = xlsxwriter.Workbook(time+'_snapshot_report.xlsx')
        bold = workbook.add_format({'bold': True})
        sql = """SELECT kid, vm, rp, folder, snapshotDate FROM (
                select * from vmwaresnapshots where kid NOT IN ("K1074","K1014","K1009","K1039") or kid IS NULL
                ) z_q where snapshotName IN ("VEEAM BACKUP TEMPORARY SNAPSHOT") and cid like %s ORDER BY z_q.cid DESC,z_q.kid DESC;"""
        mycursor.execute(sql, (lastconfig,))
        result = mycursor.fetchall()

        worksheet_vms = workbook.add_worksheet("snapshot")
        worksheet_vms.write(0, 0, 'KID', bold)
        worksheet_vms.write(0, 1, 'VM', bold)
        worksheet_vms.write(0, 2, 'Resource Pool', bold)
        worksheet_vms.write(0, 3, 'Folder', bold)
        worksheet_vms.write(0, 4, 'Snapshot Date', bold)
        idx = 0
        for row in result:
            worksheet_vms.write(idx+1, 0, row[0])
            worksheet_vms.write(idx+1, 1, row[1])
            worksheet_vms.write(idx+1, 2, row[2])
            worksheet_vms.write(idx+1, 3, row[3])
            worksheet_vms.write(idx+1, 4, row[4])
            idx+=1

        workbook.close()

        with open('dnevni_snapshot_report.txt', 'w') as f:
            for row in result:
                print(row)
                f.write("%s\n\n" % str(row))

def main(mycursor):
    snapshot_report(mycursor)

if __name__ == "__main__":
    mycursor = mydb.cursor(buffered=True)
    main(mycursor)
    mydb.close()
