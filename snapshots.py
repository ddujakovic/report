import pprint
import ssl
import itertools
import operator

from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim


class snapshotclass(object):
    """Retrival and parsing IaaS data from vSphere vCenter"""

    service_instance = None
    hosts = None
    hostPgDict = None

    def __init__(self, host, user, pwd, port):
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        ssl_context.verify_mode = ssl.CERT_NONE

        self.service_instance = connect.SmartConnect(host=host,
                                                     user=user,
                                                     pwd=pwd,
                                                     port=int(port),
                                                     sslContext=ssl_context)

    def get_snapshots(self):
        content = self.service_instance.RetrieveContent()

        container = content.rootFolder  # starting point to look into
        viewType = [vim.VirtualMachine]  # object types to look for
        recursive = True  # whether we should look into it recursively
        containerView = content.viewManager.CreateContainerView(
            container, viewType, recursive)

        children = containerView.view

        snaps = []
        for virtual_machine in children:
            summary = virtual_machine.summary
            snapshot = virtual_machine.snapshot
            if hasattr(virtual_machine.resourcePool, 'name'):
                rpool = virtual_machine.resourcePool.name
            else:
                rpool = "root"

            folder = None
            if str(virtual_machine.parent).startswith("'vim.Folder"):
                folder = virtual_machine.parent.name
                vmparent = virtual_machine.parent.parent
                # rekurzivno tražiti dok parent folder nije 'vm'
                while folder != 'vm' and vmparent.name != 'vm':
                    folder = vmparent.name
                    vmparent = vmparent.parent
            else:
                # vApp je oblik resource poola
                if hasattr(virtual_machine.resourcePool, 'parentFolder'):
                    folder = virtual_machine.resourcePool.parentFolder.name
                else:
                    print("no folder, using resource pool name for KK ID! %s %s" % (
                        rpool, summary.config.name))
                    pprint.pprint(virtual_machine.resourcePool.parent)
            #print(virtual_machine.snapshot.rootSnapshotList[0])
            if hasattr(virtual_machine.snapshot, 'currentSnapshot'):
                snaps.append({
                    'uuid': summary.config.instanceUuid,
                    'vm': summary.config.name,
                    'rp': rpool,
                    'folder': folder,
                    'snapshotDate': snapshot.rootSnapshotList[0].createTime,
                    'snapshotName': snapshot.rootSnapshotList[0].name
                })

        snaps = sorted(snaps, key=lambda k: k['rp'])
        return snaps

    def sort_uniq(self, sequence, key):
        return map(
            operator.itemgetter(0),
            itertools.groupby(sorted(sequence, key=lambda k: k[key])))

    def disconnect(self):
        """ disconnect """
        connect.Disconnect(self.service_instance)
