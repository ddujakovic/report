import requests
import sys
import os
import xml.etree.ElementTree as ET
import base64
import mysql.connector
from datetime import datetime, date, timedelta
import config
import re
import urllib3
import csv

###Disable HTTPS warnings https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

namespacename = re.compile(r'/^$|\s+/')

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )
systems=[]
#hcpsystem=sys.argv[1]
hcpsystems=int(len(sys.argv))
#print(hcpsystems)
if hcpsystems == 2:
    systems.append(hcpsystem)
elif hcpsystems > 2:
    i=1
    for i in range(1,hcpsystems):
        systems.append(sys.argv[i])
    #print(systems)

#basedir="/home/ddujakovic/report/hcpcbc/_hcpcbc.dir/"+hcpsystem+"/"
#clientsdir=os.chdir(basedir)
#clients=([ name for name in os.listdir(basedir) if os.path.isdir(os.path.join(basedir, name))])

def prepare_tables(mycursor):
    ##################
    ###dnevnireport###
    ##################
    mycursor.execute("SELECT MAX(id) FROM hcpreport")
    id=mycursor.fetchone()[0]
    if id:
        id += 1
    else:
        id = 1
    
    sql= """INSERT INTO hcpreport (id, created) VALUES ( %s, NOW())"""
    data=(id,)
    mycursor.execute(sql, data)
    return id

def do_hcpclients(mycursor,suffix):
    for index in range(len(systems)):
        basedir="/home/ddujakovic/report/hcpcbc/_hcpcbc.dir/"+systems[index]+"/"
        hcpsys=systems[index]
        clientsdir=os.chdir(basedir)
        clients=([ name for name in os.listdir(basedir) if os.path.isdir(os.path.join(basedir, name))])
        for index in range(len(clients)):
            tenant=(clients[index])
            workingdir=basedir+"/"+tenant+"/__raw/total/"
            workingdir=os.chdir(workingdir)
            #print(os.getcwd())
            csvfile=os.listdir(workingdir)
            csvfile=csvfile[0]
            #print(csvfile)
            print("WORKING ON - "+tenant+" on HCP "+hcpsys)
            with open(csvfile) as csv_file:
                csv_reader = csv.DictReader(csv_file, delimiter=',')
                for lines in csv_reader:
                    namespaceName=lines['namespaceName']

                    ingestedVolume=int(lines['ingestedVolume'])/1024/1024 #MB
                    #print(ingestedVolume)
                    storageCapacityUsed=int(lines['storageCapacityUsed'])/1024/1024 #MB
                    #print(storageCapacityUsed)
                    bucketReads=int(lines['reads'])
                    bucketWrites=int(lines['writes'])
                    bucketDeletes=int(lines['deletes'])
                    
                    sql = """INSERT INTO hcp (cid, systemName, tenantName, namespaceName, objectCount, ingestedVolume, storageCapacityUsed, bucketReads, bucketWrites, bucketDeletes) VALUES (%s, %s, %s, %s, %s, ROUND(%s,2), ROUND(%s,2), %s, %s, %s)"""
                    
                    m = namespacename.match(namespaceName)
                    if namespaceName == "":
                        namespaceName=tenant+" TOTAL"
                    mycursor.execute(sql, (suffix, lines['systemName'], lines['tenantName'], namespaceName, lines['objectCount'], ingestedVolume, storageCapacityUsed, lines['reads'], lines['writes'], lines['deletes']))

def do(mycursor):
    suffix = prepare_tables(mycursor)
    do_hcpclients(mycursor,suffix)

if __name__ == "__main__":
    mycursor = mydb.cursor()
    do(mycursor)
    mydb.commit()
    mydb.close()
    
