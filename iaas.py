import mysql.connector
from mysql.connector import Error
from datetime import datetime
import config
import iaasreportvmw
import iaasreporthcs
import iaasreportops
import veeamvsppreport
import usagemeterreport
import re
import commvault
import snapshots


mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )

#mycursor = mydb.cursor()
time=datetime.now()
time=time.strftime("%d.%m.%Y %H:%M")
re_kid = re.compile(r'.*(K\d\d\d\d)')

def prepare_tables(mycursor):
    ##################
    ###dnevnireport###
    ##################
    mycursor.execute("CREATE TABLE IF NOT EXISTS dnevnireport (id INT AUTO_INCREMENT PRIMARY KEY, created VARCHAR(255))")
    mycursor.execute("SELECT MAX(id) FROM dnevnireport")
    id=mycursor.fetchone()[0]
    if id:
        id += 1
    else:
        id = 1
    
    insertdata= """INSERT INTO dnevnireport (id, created) VALUES ( %s, NOW())"""
    data=(id,)
    mycursor.execute(insertdata, data)
    ############
    ###vmware###
    ############
    mycursor.execute("CREATE TABLE IF NOT EXISTS vmware (cid INT, kid VARCHAR(255),vcenter VARCHAR(255), rp VARCHAR(255),vm VARCHAR(255), ram INT, vcpu INT, power_state VARCHAR(255),folder VARCHAR(255), annotation TEXT, storage VARCHAR(255), storage_uncommited VARCHAR(255), os_type VARCHAR(255))")
    ###############
    ###snapshots###
    ###############
    mycursor.execute("CREATE TABLE IF NOT EXISTS vmwaresnapshots (cid INT, kid VARCHAR(255),vcenter VARCHAR(255), rp VARCHAR(255),vm VARCHAR(255), power_state VARCHAR(255), snapshots VARCHAR(255), folder VARCHAR(255))")
    #####################
    ###vmwaredatastore###
    #####################
    mycursor.execute("CREATE TABLE IF NOT EXISTS vmwaredatastore (cid INT, vcenter VARCHAR(255), kid VARCHAR(255), name VARCHAR(255), capacitygb DOUBLE, freegb DOUBLE)")
    ###################
    ###vmwarenetwork###
    ####################
    mycursor.execute("CREATE TABLE IF NOT EXISTS vmwarenetwork (cid INT, vcenter VARCHAR(255), kid VARCHAR(255), portgroup VARCHAR(255), vlan VARCHAR(255))")
    #################
    ###storageluns###
    #################
    mycursor.execute("CREATE TABLE IF NOT EXISTS storageluns (cid INT, storage VARCHAR(255), kid VARCHAR(255), ldev VARCHAR(255), name VARCHAR(255), capacitygb DOUBLE, consumedgb DOUBLE, pool_id INT, tier0gb DOUBLE, tier1gb DOUBLE, tier2gb DOUBLE)")
    
    return id

#####################################################
###gleda iaasreportvmw.py i upisuje podatke u bazu###
#####################################################
def do_vmware(mycursor,suffix):
    for addr, desc in config.cfg.vcenters.items():
        print(desc)
        vmw = iaasreportvmw.IaaSReportVMW(addr,config.cfg.USERNAME,config.cfg.PASSWORD,443)
        
        vms = vmw.get_vms()

        ### VMWARE RESURSI vcpu,ram,...
        sql = """INSERT INTO vmware (cid, kid, vcenter, rp, vm, ram, vcpu, power_state, folder, annotation, storage, storage_uncommited, os_type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

        for item in vms:
            # item['folder'] za suffix ima ID korisnika, Kxxxx
            # šifra u folderu umjesto resource poolu
            m = re_kid.match(item['folder'])
            kid = None
            if m:
                kid= m.group(1)
            else:
                m = re_kid.match(item['rp'])
                if m:
                    kid = m.group(1)

            mycursor.execute(sql, (suffix, kid, desc, item['rp'], item['vm'], item['ram'], item['vcpu'], item['power_state'], item['folder'], item['annotation'], item['storage'], item['storage_uncommited'], item['os_type']))

        ### VMWARE VM koje imaju snapshot...
        #snapshots = vmw.get_snapshots()
        #sql = """INSERT INTO vmwaresnapshots (cid, kid, vcenter, rp, vm, power_state, folder) 
       #         VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        #for item in snapshots:
       #     # item['folder'] za suffix ima ID korisnika, Kxxxx
        #    # šifra u folderu umjesto resource poolu
       #     m = re_kid.match(item['folder'])
       #     kid = None
       #     if m:
       ##         kid= m.group(1)
       #     else:
        #        m = re_kid.match(item['rp'])
        #        if m:
        #            kid = m.group(1)
        #    mycursor.execute(sql, (suffix, kid, desc, item['rp'], item['vm'], item['power_state'], item['folder']))

        ### VMWARE DATASTORES
        datastores = vmw.get_datastores()
        sql = """INSERT INTO vmwaredatastore ( cid, vcenter, kid, name, capacitygb, freegb ) VALUES ( %s, %s, %s, %s, %s, %s )"""
        for item in datastores:
            # item['name'] za suffix ima ID korisnika, Kxxxx
            m = re_kid.match(item['name'])
            kid = None
            if m:
                kid = m.group(1)
            mycursor.execute(sql, (suffix, desc, kid, item['name'], item['capacity'], item['free']))

        ### VMWARE VLANS
        vlans = vmw.get_vlans()
        sql = """ INSERT INTO vmwarenetwork ( cid, vcenter, kid, portgroup, vlan )
                    VALUES ( %s, %s, %s, %s, %s ) """
        for name in vlans:
            # vid portgrupa name, suffix ima ID korisnika, Kxxxx
            m = re_kid.match(name)
            kid = None
            if m:
                kid = m.group(1)
            mycursor.execute( sql, (suffix, desc, kid, name, vlans[name]))

        vmw.disconnect()

#####################################################
###gleda iaasreporthcs.py i upisuje podatke u bazu###
#####################################################
def do_storageluns(mycursor,suffix):
    print("HITACHI COMMAND SUITE START")
    hcs = iaasreporthcs.IaaSReportHCS()
    hcs.gethosts()
    storages = hcs.getarrays()
  
    for stor in storages:
        print("stor %s" % stor)

        luns = hcs.getluns(stor['arrayType'], stor['serialNumber'])

        sql = """INSERT INTO storageluns (cid, storage, kid, ldev, name, capacitygb, consumedgb, pool_id, tier0gb, tier1gb, tier2gb) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

        for item in luns:
            m = re_kid.match(item['label'])
            kid = None
            if m:
                kid = m.group(1)
			
            mycursor.execute(sql, (suffix, "%s-%s" % (stor['displayArrayType'], stor['serialNumber']), kid, item['displayName'], item['label'], item['size'], item['consumed'], item['dpPoolID'], item['t0consumed'], item['t1consumed'], item['t2consumed']))
    print("HITACHI COMMAND SUITE DONE")
#####################################################
###gleda iaasreportops.py i upisuje podatke u bazu###
#####################################################

def do_hitachi_ops(mycursor,suffix):
    print("HITACHI OPS START")
    ops = iaasreportops.iaasreportOPS()
    storages = ops.getstorages()
    #print(storages)
    for store in storages:
        #print("Storage system: ", store.get("storageSystemName"))
        #print("Storage system: ", store.get("storageSystemId"))
        storage = store.get("storageSystemId")
        print(storage)
        luns = ops.getluns(storage)

        sql = """INSERT INTO storages (cid, storagename, storageid, ldev, kid, volname, capacitygb, consumedgb) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""
        for item in luns:
            #print(luns)
            m = re_kid.match(item['displayName'])
            kid = None
            if m:
                kid = m.group(1)
                #print(store['storageSystemName'])
                #print(store['storageSystemId'])
                #print(item['displayName'])
                #print(kid)
                #print(item['size'])
                #print(item['consumed'])
                #print(item['tiername'])
            mycursor.execute(sql, (suffix, store['storageSystemName'], store['storageSystemId'], item['ldev'], kid, item['displayName'], item['size'], item['consumed']))
    print("HITACHI OPS DONE")
#######################################################
###gleda veeamvsppreport.py i upisuje podatke u bazu###
#######################################################
def do_veeamvspp(mycursor,suffix):
    veeam = veeamvsppreport.veeamvspp()
    veeamjobs = veeam.getveeamvsppjobs()
    veeambackupfiles = veeam.getveeamvsppbackupfiles()
    veeambackupfiledetails = veeam.getbackupfiledetails(veeambackupfiles)
    veeambackupid= veeam.getveeambackupid()
    veeambackupvmcount = veeam.getveeambackupvmcount(veeambackupid)
    #print(veeambackupfiledetails)
    sql = """INSERT INTO veeambackupjobs (cid, kid, jobname) VALUES (%s, %s, %s)"""
    for item in veeamjobs:
        print("Backup Job: ", item.get("JobName"))
        m = re_kid.match(item['JobName'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['JobName']))

    sql = """INSERT INTO veeambckfiles (cid, bckfilehref) VALUES (%s, %s)"""
    for item in veeambackupfiles:
        mycursor.execute(sql, (suffix, item['BackupFile']))
    
    sql = """INSERT INTO veeambckdetail (cid, kid, bckfilename, bckfilesize, bckfilepath) VALUES (%s, %s, %s, %s, %s)"""
    for item in veeambackupfiledetails:
        m = re_kid.match(item['FilePath'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['Name'], item['BackupSize'], item['FilePath']))
    
    sql = """INSERT INTO veeamvmcount (cid, kid, vmcount) VALUES (%s, %s, %s)"""
    for item in veeambackupvmcount:
        #print(item['JobName'])
        m = re_kid.match(item['JobName'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['VMCount']))

def do_usagemeterreport(mycursor, suffix):
    usagemeter = usagemeterreport.usagemeter()
    #usagemetercustomers = usagemeterreport.getcustomers()
    usagemeterdata = usagemeter.getcustomerusage()
    print("USAGE METER REPORT...")
    sql = """INSERT INTO usagemeter (cid, kid, usagepoints) VALUES (%s, %s, %s)"""
    for item in usagemeterdata:
        mycursor.execute(sql, (suffix, item['customer'], item['usageGB']))
    print("USAGE METER REPORT DONE!!!")
###
###Gleda config.py cfg.korisnici i upisuje ih u tablicu korisnici
###ZAKOMENTIRANO, KORISNICI SE UNOSE DIREKTNO NA STRANICI /novikorisnik
#def do_korisnici():
#    sql = """INSERT IGNORE INTO korisnici (id, korisnik) VALUES (%s, %s)"""
#    for k in config.cfg.korisnici:
#        mycursor.execute(sql, (k, config.cfg.korisnici[k]))
#    mydb.commit()

def do_commvault(mycursor,suffix):
    print("COMMVAULT START")
    comm=commvault.commv()
    #client name, id, group, K$$$$
    commvault_clients=comm.client_id()
    #agent name, os, group, license
    commvault_client_details=comm.clientdetails()
    sql = """INSERT INTO commvaultkorisnici (cid, kid, commname, commgroupid, commgroup) values (%s, %s, %s, %s, %s)"""
    for item in commvault_clients:
        m = re_kid.match(item['Description'])
        kid = None
        if m:
            kid = m.group(1)
        mycursor.execute(sql, (suffix, kid, item['ClientName'], item['ClientID'], item['CommvaultGroup']))
    
    sql = """INSERT INTO commvaultagenti (cid, clientdisplayname, clientos, commgroupid, clientlicense, isdeletedclient) values (%s, %s, %s, %s, %s, %s)"""
    for item in commvault_client_details:
        mycursor.execute(sql, (suffix, item['clientDisplayName'], item['clientOSName'], item['clientCommvaultGroupId'], item['clientLicenses'], item['isdeletedclient']))

    #commvault library usage
    commvault_libraries=comm.commvaultlibraries()
    commvault_libusage=comm.commvaultlibraryusage(commvault_libraries)
    sql = """INSERT INTO commvaultlibraryusage (cid, libname, libid, libtotal, libavailable) values (%s, %s, %s, %s, %s)"""
    for item in commvault_libusage:
        mycursor.execute(sql, (suffix, item['LibraryName'], item['LibraryID'], item['LibraryTotal'], item['LibraryAvailable']))
   
    print("COMMVAULT DONE")

########################
### VMWARE SNAPSHOTS ###
########################
def do_snapshots(mycursor, suffix):
    for addr, desc in config.cfg.vcenters.items():
        print("Snapshots for "+desc)
        snaps = snapshots.snapshotclass(addr,config.cfg.USERNAME,config.cfg.PASSWORD,443)
        snapshot = snaps.get_snapshots()

        ### VMWARE RESURSI vcpu,ram,...
        sql = """INSERT INTO vmwaresnapshots (cid, kid, uuid, vm, rp, folder, snapshotDate, snapshotName) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

        for item in snapshot:
            # item['folder'] za suffix ima ID korisnika, Kxxxx
            # šifra u folderu umjesto resource poolu
            m = re_kid.match(item['folder'])
            kid = None
            if m:
                kid= m.group(1)
            else:
                m = re_kid.match(item['rp'])
                if m:
                    kid = m.group(1)

            mycursor.execute(sql, (suffix, kid, item['uuid'], item['vm'], item['rp'], item['folder'], item['snapshotDate'], item['snapshotName']))

        snaps.disconnect()

def do(mycursor):
    suffix = prepare_tables(mycursor)
    #do_korisnici()
    try:
        do_vmware(mycursor,suffix)
    except Exception as e: print(e)
    try:
        do_storageluns(mycursor,suffix)
    except Exception as e: print(e)
    try:
        do_hitachi_ops(mycursor,suffix)
    except Exception as e: print(e)
    try:
        do_veeamvspp(mycursor,suffix)
    except Exception as e: print(e)
    try:
        do_usagemeterreport(mycursor, suffix)
    except Exception as e: print(e)
    try:
        do_commvault(mycursor, suffix)
    except Exception as e: print(e)
    try:
        do_snapshots(mycursor, suffix)
    except Exception as e: print(e)

if __name__ == "__main__":
    mycursor = mydb.cursor()
    do(mycursor)
    mydb.commit()
    mydb.close()
