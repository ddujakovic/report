#!/usr/bin/python
from flask import Flask, render_template, request, jsonify, redirect, json, flash, url_for, send_file
from flask_mysqldb import MySQL, MySQLdb
import xlsxwriter
import csv
import config
import io
from decimal import Decimal
from pychartjs import BaseChart, ChartType, Color

app = Flask(__name__)
app.secret_key = "x7UMMa*n8<{6{h^Gt\kaf-=5\KqdPY"

app.config['MYSQL_HOST'] = config.cfg.dbhost
app.config['MYSQL_USER'] = config.cfg.dbuser
app.config['MYSQL_PASSWORD'] = config.cfg.dbpass
app.config['MYSQL_DB'] = config.cfg.database

mysql = MySQL(app)

@app.route("/")
def config():
        mycursor = mysql.connection.cursor()
        getid = mycursor.execute("SELECT * FROM report.dnevnireport WHERE id in (select max(id) from dnevnireport)")
        #config = mycursor.execute("SELECT * FROM dnevnireport where id in (@getid)")
        if config:
            lastconfig = mycursor.fetchone()
            return render_template('index.html',lastconfig=lastconfig)
#################
### KORISNICI ###
#################
@app.route("/users")
def users():
        mycursor = mysql.connection.cursor()
        #mycursor = mydb.cursor()
        korisnici = mycursor.execute("SELECT * FROM korisnici")
        if korisnici:
            userDetails = mycursor.fetchall()
            return render_template('users.html',userDetails=userDetails)
        #mydb.close()
##############
### VMWARE ###
##############
@app.route("/vmware", methods=['GET','POST'])
def vmware():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')

        if selected_config==None:
            mycursor.execute('''SELECT kid, vcenter, rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) FROM vmware where cid like %s''', (lastconfig,))
            lastvmware = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
        else:
            mycursor.execute('''SELECT kid, vcenter, rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) FROM vmware where cid like %s''', (selected_config,))
            lastvmware = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
        
        return render_template('vmware.html', getallconfid=getallconfid, lastvmware=lastvmware, lastdate=lastdate)

#########################
### DETALJI KORISNIKA ###
#########################
@app.route("/userdetail=<path:kid>", methods=['GET','POST'])
def userdetail(kid):
        mycursor = mysql.connection.cursor()
                
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) FROM dnevnireport")
        lastconfig=mycursor.fetchone()
        

        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()

        #Dohvaca odabranu konfu iz select forme na /userdetail i vraća 
        global selected_config
        selected_config = request.form.get('config_id')
        #pri otvaranju stranice nema odabrane konfe, uzima zadnje generirane podatke
        if selected_config==None:
            #VMWARE_TABLE
            mycursor.execute('''SELECT kid, vcenter, rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, 
            annotation, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) 
            FROM vmware where cid like %s and kid like %s''', (lastconfig, kid))
            lastvmware = mycursor.fetchall()
            #CONFIG_SELECT
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
            #vmware_datastore_table
            mycursor.execute('''SELECT vcenter, name, capacitygb, ROUND(freegb,2) 
            FROM vmwaredatastore WHERE cid like %s and kid like %s''', (lastconfig, kid,))
            datastores = mycursor.fetchall()
            #hus_volumeni
            mycursor.execute('''SELECT storage, name, n.capacitygb, ROUND(n.consumedgb,2) 
                                FROM storageluns n 
                                WHERE cid like %s and kid like %s and storage IN ('HUS VM-240270','HUS110-91257788','HUS130-92256025');''', (lastconfig, kid,))
            hus_luns = mycursor.fetchall()
            #ops_volumeni
            mycursor.execute('''SELECT storagename, volname, n.capacitygb, ROUND(n.consumedgb,2) 
                                FROM storages n 
                                WHERE cid like %s and kid like %s;''', (lastconfig, kid,))
            ops_luns = mycursor.fetchall()
            #vmware_network_table
            mycursor.execute('''SELECT vcenter, portgroup, vlan 
            FROM vmwarenetwork WHERE cid like %s and kid like %s''', (lastconfig, kid,))
            networks = mycursor.fetchall()
            #veeamvspp vm count
            mycursor.execute('''SELECT DISTINCT kid, vmcount FROM veeamvmcount
            WHERE cid like %s and kid like %s''', (lastconfig, kid,))
            veeamvms = mycursor.fetchall()
            #veeambackupsize
            mycursor.execute('''SELECT kid, sum(ROUND(bckfilesize/1024/1024/1024)) FROM veeambckdetail
            WHERE cid like %s and kid like %s group by veeambckdetail.kid''', (lastconfig, kid,))
            veeambackupsize = mycursor.fetchall()
        else:
            #VMWARE_TABLE
            mycursor.execute('''SELECT kid, vcenter, rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, 
            annotation, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5)
            FROM vmware where cid like %s and kid like %s''', (selected_config, kid,))
            lastvmware = mycursor.fetchall()
            #CONFIG_SELECT
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
            #vmware_datastore_table
            mycursor.execute('''SELECT vcenter, name, capacitygb, ROUND(freegb,2) 
            FROM vmwaredatastore WHERE cid like %s and kid like %s''', (selected_config, kid,))
            datastores = mycursor.fetchall()
            #hus_volumeni
            mycursor.execute('''SELECT storage, name, n.capacitygb, ROUND(n.consumedgb,2) 
                                FROM storageluns n 
                                WHERE cid like %s and kid like %s and storage IN ('HUS VM-240270','HUS110-91257788','HUS130-92256025');''', (selected_config, kid,))
            hus_luns = mycursor.fetchall()
            #ops_volumeni
            mycursor.execute('''SELECT storagename, volname, n.capacitygb, ROUND(n.consumedgb,2) 
                                FROM storages n 
                                WHERE cid like %s and kid like %s;''', (selected_config, kid,))
            ops_luns = mycursor.fetchall()
            #vmware_network_table
            mycursor.execute('''SELECT vcenter, portgroup, vlan 
            FROM vmwarenetwork WHERE cid like %s and kid like %s''', (selected_config, kid,))
            networks = mycursor.fetchall()
            #veeamvspp vm count
            mycursor.execute('''SELECT DISTINCT kid, vmcount FROM veeamvmcount
            WHERE cid like %s and kid like %s''', (selected_config, kid,))
            veeamvms = mycursor.fetchall()
            #veeambackupsize
            mycursor.execute('''SELECT kid, sum(ROUND(bckfilesize/1024/1024/1024)) FROM veeambckdetail
            WHERE cid like %s and kid like %s group by veeambckdetail.kid''', (selected_config, kid,))
            veeambackupsize = mycursor.fetchall()

        return render_template('userdetail.html', kid=kid, datastores=datastores, hus_luns=hus_luns, ops_luns=ops_luns, networks=networks, getallconfid=getallconfid, lastvmware=lastvmware, lastdate=lastdate, veeamvms=veeamvms, veeambackupsize=veeambackupsize)

########################################
#########################################
@app.route("/userdetail=<path:kid>/download", methods=['GET','POST'])
def download_report(kid):
        # Create an in-memory output file for the new workbook.
        output = io.BytesIO()
        mycursor = mysql.connection.cursor()
        #selected_config = request.form.get('config_id')        
        if selected_config==None:
            mycursor.execute("SELECT MAX(id) FROM dnevnireport")
            lastconfig=mycursor.fetchone()
            mycursor.execute('''SELECT rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) 
                        FROM vmware where cid like %s and kid like %s''', (lastconfig, kid))
            result = mycursor.fetchall()
            #print("NONE")
        else:
            mycursor.execute('''SELECT rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) 
                        FROM vmware where cid like %s and kid like %s''', (selected_config, kid))
            result = mycursor.fetchall()
            #print("SELECTED")
        #create workbook and sheet
        #Kreira u memoriji, ne sprema lokalno na server
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        bold = workbook.add_format({'bold': True})
        worksheet_vms = workbook.add_worksheet('VMs')
            #Headers
        worksheet_vms.write(0, 0, 'Resource Pool', bold)
        worksheet_vms.write(0, 1, 'VM', bold)
        worksheet_vms.write(0, 2, 'RAM(GB)', bold)
        worksheet_vms.write(0, 3, 'VCPU', bold)
        worksheet_vms.write(0, 4, 'Power State', bold)
        worksheet_vms.write(0, 5, 'Folder', bold)
        worksheet_vms.write(0, 6, 'OS Type', bold)
        worksheet_vms.write(0, 7, 'Disk Size(GB)', bold)
        worksheet_vms.write(0, 8, 'Disk Free(GB)', bold)
        
            #Write data
        idx = 0
        for row in result:
            worksheet_vms.write(idx+1, 0, row[0])
            worksheet_vms.write(idx+1, 1, row[1])
            worksheet_vms.write(idx+1, 2, row[2])
            worksheet_vms.write(idx+1, 3, row[3])
            worksheet_vms.write(idx+1, 4, row[4])
            worksheet_vms.write(idx+1, 5, row[5])
            worksheet_vms.write(idx+1, 6, row[6])
            worksheet_vms.write(idx+1, 7, row[7])
            worksheet_vms.write(idx+1, 8, row[8])
            idx+=1
        limitvcpu='D'+str(idx+1)
        limitram='C'+str(idx+1)
        worksheet_vms.write(idx+2, 0, 'VMWARE SUMMARY', bold)
        worksheet_vms.write(idx+3, 0, 'VCPU', bold)
        worksheet_vms.write(idx+3, 1, 'VRAM', bold)
        worksheet_vms.write(idx+4, 0, '{=SUM(D2:'+limitvcpu+')}' )
        worksheet_vms.write(idx+4, 1, '{=SUM(C2:'+limitram+')}' )

        
            # Close the workbook before streaming the data
        workbook.close()
            # Rewind the buffer
        output.seek(0)
        return send_file(output, attachment_filename=kid+'_'+selected_config+"_report.xlsx", as_attachment=True)

#########################################
#########################################
#################
### DATASTORE ###
#################
@app.route("/datastore", methods=['GET','POST'])
def datastore():
        mycursor = mysql.connection.cursor()    
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')

        if selected_config==None:
            mycursor.execute('''SELECT kid, vcenter, name, capacitygb, ROUND(freegb,2) 
            FROM vmwaredatastore WHERE cid like %s''', (lastconfig,))
            datastores = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
        else:
            mycursor.execute('''SELECT kid, vcenter, name, capacitygb, ROUND(freegb,2) 
            FROM vmwaredatastore WHERE cid like %s''', (selected_config,))
            datastores = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()

        return render_template('datastore.html', getallconfid=getallconfid, datastores=datastores, lastdate=lastdate)

################
### HUS LUNS ###
################
@app.route("/storageluns", methods=['GET','POST'])
def storageluns():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')

        if selected_config==None:
            mycursor.execute('''SELECT korisnici.id, korisnici.korisnik, storage, ldev, name, capacitygb, consumedgb, pool_id, tier0gb, tier1gb, tier1gb FROM storageluns
left join korisnici on korisnici.id=storageluns.kid where cid like %s and storage IN ('HUS VM-240270','HUS110-91257788','HUS130-92256025');''', (lastconfig,))
            storageluns = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
        else:
            mycursor.execute('''SELECT korisnici.id, korisnici.korisnik, storage, ldev, name, capacitygb, consumedgb, pool_id, tier0gb, tier1gb, tier1gb FROM storageluns
left join korisnici on korisnici.id=storageluns.kid where cid like %s and storage IN ('HUS VM-240270','HUS110-91257788','HUS130-92256025');''', (selected_config,))
            storageluns = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
        
        return render_template('storageluns.html', getallconfid=getallconfid, storageluns=storageluns, lastdate=lastdate)
################
### OPS LUNS ###
################
@app.route("/opsluns", methods=['GET','POST'])
def opsluns():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')

        if selected_config==None:
            mycursor.execute('''SELECT korisnici.id, korisnici.korisnik, storagename, storageid, ldev, volname, capacitygb, consumedgb FROM storages left join korisnici on korisnici.id=storages.kid where cid like %s;''', (lastconfig,))
            storageluns = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
        else:
            mycursor.execute('''SELECT korisnici.id, korisnici.korisnik, storagename, storageid, ldev, volname, capacitygb, consumedgb FROM storages left join korisnici on korisnici.id=storages.kid where cid like %s;''', (selected_config,))
            storageluns = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
        
        return render_template('opsluns.html', getallconfid=getallconfid, storageluns=storageluns, lastdate=lastdate)
#############
### VEEAM ###
#############
@app.route("/veeamvspp", methods=['GET','POST'])
def veeamvspp():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')

        if selected_config==None:
            mycursor.execute('''SELECT DISTINCT veeambackupjobs.kid, jobname, veeambckdetail.bckfilename, ROUND(veeambckdetail.bckfilesize/1024.0/1024/1024,2) FROM veeambackupjobs
LEFT JOIN  veeambckdetail ON veeambackupjobs.kid=veeambckdetail.kid where veeambckdetail.cid like %s
ORDER BY veeambackupjobs.kid DESC;''', (lastconfig,))
            veeambackup = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
            #veeambackupsize
            mycursor.execute('''select kid, sum(ROUND(veeambckdetail.bckfilesize/1024/1024/1024)) from veeambckdetail where cid like %s group by kid;''', (lastconfig, ))
            veeambackupsize = mycursor.fetchall()
        else:
            mycursor.execute('''SELECT DISTINCT veeambackupjobs.kid, jobname, veeambckdetail.bckfilename, ROUND(veeambckdetail.bckfilesize/1024.0/1024/1024,2) FROM veeambackupjobs
LEFT JOIN  veeambckdetail ON veeambackupjobs.kid=veeambckdetail.kid where veeambckdetail.cid like %s
ORDER BY veeambackupjobs.kid DESC;''', (selected_config,))
            veeambackup = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
            #veeambackupsize
            mycursor.execute('''select kid, sum(ROUND(veeambckdetail.bckfilesize/1024/1024/1024)) from veeambckdetail where cid like %s group by kid;''', (selected_config, ))
            veeambackupsize = mycursor.fetchall()

        return render_template('veeamvspp.html', getallconfid=getallconfid, veeambackup=veeambackup, lastdate=lastdate, veeambackupsize=veeambackupsize)




@app.route("/test",methods=['GET','POST'])
def test():
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastdate=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        mycursor.execute("select id from (select id from dnevnireport ORDER BY id DESC LIMIT 30) tmp order by id asc;")
        labels = mycursor.fetchall()
        mycursor.execute("select LEFT(created,LENGTH(created)-9) from (select created from dnevnireport ORDER BY id DESC LIMIT 30) tmp order by created asc;")
        labels_dani = mycursor.fetchall()
        novelabele=[]
        #print(labels)
        for item in range(len(labels_dani)):
            #print(labels_dani[item])
            novelabele.append(str(labels_dani[item])[2:-3])
        #print(novelabele)
        values=[]
        for item in labels:
            mycursor.execute("SELECT SUM((ram/1024)) from vmware where kid like 'K1009' AND cid like %s;", (item))
            data = mycursor.fetchone()
            if data[0] is None:
                values.append(0)
            else:
                values.append(float(data[0]))
        #print(values)
        valuesvcpu=[]
        for item in labels:
            mycursor.execute("SELECT SUM(vcpu) from vmware where kid like 'K1009' AND cid like %s;", (item))
            data = mycursor.fetchone()
            if data[0] is None:
                valuesvcpu.append(0)
            else:
                valuesvcpu.append(float(data[0]))
        #print(valuesvcpu)
        return render_template('test.html', getallconfid=getallconfid, lastdate=lastdate, novelabele=novelabele, values=values, valuesvcpu=valuesvcpu)

########################
### OBRISI KORISNIKA ###
########################
@app.route("/obrisikorisnika=<path:kid>",methods=['GET','POST'])
def brisanjekorisnika(kid):
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT * FROM korisnici where id like %s", (kid,))
        user=mycursor.fetchone()
        if request.method == "POST":
            if request.form['obrisi'] == 'Delete':
                sifrakorisnika = request.form['id']
                mycursor.execute("DELETE FROM korisnici WHERE id=%s", (sifrakorisnika, ))
                mysql.connection.commit()
                mycursor.close()
                flash('Uspješno obrisan korisnik')
                return redirect('https://reporter.setcor.com/users')
            elif request.form['obrisi'] == 'Cancel':
                return redirect('https://reporter.setcor.com/users')
        return render_template('obrisikorisnika.html', user=user)

@app.route("/uredikorisnika=<path:kid>", methods=['GET','POST'])
def uredikorisnika(kid):
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT * FROM korisnici where id like %s", (kid,))
        user=mycursor.fetchone()
        if request.method == "POST":
            sifrakorisnika = request.form['id']
            imekorisnika = request.form['korisnik']
            mycursor.execute("UPDATE korisnici SET korisnik=%s WHERE id=%s", (imekorisnika, sifrakorisnika))
            mysql.connection.commit()
            mycursor.close()
            flash('Uspješno uređen korisnik')
            return redirect('https://reporter.setcor.com/users')
        return render_template('uredikorisnika.html', user=user)

#######################
### DODAJ KORISNIKA ###
#######################
@app.route("/novikorisnik",methods=['GET','POST'])
def unoskorisnika():
        mycursor = mysql.connection.cursor()
        if request.method == "POST":
            if request.form['dodaj'] == 'Add':
                sifrakorisnika = request.form['id']
                imekorisnika = request.form['korisnik']
                mycursor.execute("INSERT INTO korisnici (id,korisnik) VALUES (%s, %s)", (sifrakorisnika, imekorisnika))
                mysql.connection.commit()
                mycursor.close()
                flash('Uspješno dodan novi korisnik')
                return redirect('https://reporter.setcor.com/users')
            elif request.form['dodaj'] == 'Cancel':
                return redirect('https://reporter.setcor.com/users')
        return render_template('novikorisnik.html')

####################################
###Graf potrošnje zadnjih 30 dana###
####################################
@app.route("/graph=<path:kid>", methods=['GET','POST'])
def graph(kid):
    mycursor = mysql.connection.cursor()
    mycursor.execute("select id from (select id from dnevnireport ORDER BY id DESC LIMIT 30) tmp order by id asc;")
    labels = mycursor.fetchall()
    mycursor.execute("select LEFT(created,LENGTH(created)-9) from (select created from dnevnireport ORDER BY id DESC LIMIT 30) tmp order by created asc;")
    labels_dani = mycursor.fetchall()
    novelabele=[]
    #print(labels)
    for item in range(len(labels_dani)):
        #print(labels_dani[item])
        novelabele.append(str(labels_dani[item])[2:-3])
    #print(novelabele)
    values=[]
    for item in labels:
        mycursor.execute("SELECT SUM((ram/1024)) from vmware where kid like %s AND cid like %s;", (kid,item))
        data = mycursor.fetchone()
        if data[0] is None:
            values.append(0)
        else:
            values.append(float(data[0]))
    #print(values)
    valuesvcpu=[]
    for item in labels:
        mycursor.execute("SELECT SUM(vcpu) from vmware where kid like %s AND cid like %s;", (kid,item))
        data = mycursor.fetchone()
        if data[0] is None:
            valuesvcpu.append(0)
        else:
            valuesvcpu.append(float(data[0]))
    #print(valuesvcpu)
    return render_template('graph.html', novelabele=novelabele, values=values, valuesvcpu=valuesvcpu)

@app.route("/usagemeter", methods=['GET','POST'])
def usagemeter():
        mycursor = mysql.connection.cursor()
        mycursor.execute('''select korisnici.id, korisnici.korisnik, usagemeter.usagepoints from korisnici
                            left join usagemeter ON korisnici.id = usagemeter.kid
                            where cid like (select max(id) from dnevnireport where day(created)=1)''')
        usagemeter = mycursor.fetchall()
        
        return render_template('usagemeter.html', usagemeter=usagemeter)

#################
### COMMVAULT ###
#################
@app.route("/commvault", methods=['GET','POST'])
def commvault():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from dnevnireport")
        lastconfig=mycursor.fetchone()
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM dnevnireport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')
        if selected_config==None:
            mycursor.execute('''select DISTINCT commvaultkorisnici.kid, commvaultkorisnici.commname, commvaultagenti.clientdisplayname, commvaultagenti.clientos, 
                                commvaultagenti.clientlicense, commvaultagenti.isdeletedclient from commvaultkorisnici
                                left join commvaultagenti on commvaultkorisnici.commgroupid = commvaultagenti.commgroupid
                                where commvaultagenti.cid like %s;''', (lastconfig, ))
            commvault = mycursor.fetchall()

            mycursor.execute('''select DISTINCT libname, libtotal, libavailable from commvaultlibraryusage where cid like %s and libname not like "FUJITSU%%";''', (lastconfig, ))
            commvaultlib = mycursor.fetchall()

            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
        else:
            mycursor.execute('''select DISTINCT commvaultkorisnici.kid, commvaultkorisnici.commname, commvaultagenti.clientdisplayname, commvaultagenti.clientos, 
                                commvaultagenti.clientlicense, commvaultagenti.isdeletedclient from commvaultkorisnici
                                left join commvaultagenti on commvaultkorisnici.commgroupid = commvaultagenti.commgroupid
                                where commvaultagenti.cid like %s;''', (selected_config, ))
            commvault = mycursor.fetchall()

            mycursor.execute('''select DISTINCT libname, libtotal, libavailable from commvaultlibraryusage where cid like %s and libname not like "FUJITSU%%";''', (selected_config, ))
            commvaultlib = mycursor.fetchall()

            mycursor.execute('''SELECT created FROM dnevnireport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
        return render_template('commvault.html', getallconfid=getallconfid, commvault=commvault, lastdate=lastdate, commvaultlib=commvaultlib)
#####################
### SSL CERT UNOS ###
#####################
@app.route("/sslcertunos", methods=['GET','POST'])
def sslcertunos():
        mycursor = mysql.connection.cursor()
        if request.method == "POST":
            if request.form['dodaj'] == 'Add':
                sslnaziv = request.form['sslnaziv']
                sslfirma = request.form['sslfirma']
                sslopis = request.form['sslopis']
                sslstart = request.form['sslstart']
                sslend = request.form['sslend']
                sslmail = request.form['sslmail']
                mycursor.execute("INSERT INTO sslcert (sslnaziv, sslfirma, sslopis, sslstart, sslend, sslmail) VALUES (%s, %s, %s, %s, %s, %s)", (sslnaziv, sslfirma, sslopis, sslstart, sslend, sslmail))
                mysql.connection.commit()
                mycursor.close()
                flash('Uspješno dodan novi SSL certifikat')
                return redirect('https://reporter.setcor.com/sslcert')
            elif request.form['dodaj'] == 'Cancel':
                return redirect('https://reporter.setcor.com/sslcert')
        return render_template('sslcertunos.html')
#####################
### SSL CERT EDIT ###
#####################
@app.route("/uredisslcert=<path:sslid>", methods=['GET','POST'])
def uredisslcert(sslid):
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT sslnaziv, sslfirma, sslopis, sslstart, sslend, sslmail FROM sslcert where sslid like %s", (sslid,))
        sslcert=mycursor.fetchone()
        if request.method == "POST":
            sslnaziv = request.form['sslnaziv']
            sslfirma = request.form['sslfirma']
            sslopis = request.form['sslopis']
            sslstart = request.form['sslstart']
            sslend = request.form['sslend']
            sslmail = request.form['sslmail']
            mycursor.execute("UPDATE sslcert SET sslnaziv=%s, sslfirma=%s, sslopis=%s, sslstart=%s, sslend=%s, sslmail=%s WHERE sslid=%s", (sslnaziv, sslfirma, sslopis, sslstart, sslend, sslmail, sslid))
            mysql.connection.commit()
            mycursor.close()
            flash('Uspješno uređen SSL certifikat')
            return redirect('https://reporter.setcor.com/sslcert')
        return render_template('uredisslcert.html', sslcert=sslcert)
#######################
### SSL CERT DELETE ###
#######################
@app.route("/obrisisslcert=<path:sslid>",methods=['GET','POST'])
def obrisisslcert(sslid):
        mycursor = mysql.connection.cursor()
        #mycursor.execute("SELECT * FROM korisnici where id like %s", (sslid,))
        #sslcert=mycursor.fetchone()
        if request.method == "POST":
            if request.form['obrisi'] == 'Delete':
                #sifrakorisnika = request.form['id']
                mycursor.execute("DELETE FROM sslcert WHERE sslid=%s", (sslid, ))
                mysql.connection.commit()
                mycursor.close()
                return redirect('https://reporter.setcor.com/sslcert')
            elif request.form['obrisi'] == 'Cancel':
                return redirect('https://reporter.setcor.com/sslcert')
        return render_template('obrisisslcert.html')
###############
### SSLcert ###
###############
@app.route("/sslcert", methods=['GET','POST'])
def sslcert():
        mycursor = mysql.connection.cursor()
        mycursor.execute('''SELECT sslnaziv, sslfirma, sslopis, sslstart, sslend, sslmail, isexpired, sslid FROM sslcert''')
        sslcert = mycursor.fetchall()
        return render_template('sslcert.html', sslcert=sslcert)

####################
### LICENCE UNOS ###
####################
@app.route("/licenceunos", methods=['GET','POST'])
def licenceunos():
        mycursor = mysql.connection.cursor()
        if request.method == "POST":
            if request.form['dodaj'] == 'Add':
                licnaziv = request.form['licnaziv']
                licfirma = request.form['licfirma']
                licopis = request.form['licopis']
                licstart = request.form['licstart']
                licend = request.form['licend']
                licmail = request.form['licmail']
                mycursor.execute("INSERT INTO licence (licnaziv, licfirma, licopis, licstart, licend, licmail) VALUES (%s, %s, %s, %s, %s, %s)", (licnaziv, licfirma, licopis, licstart, licend, licmail))
                mysql.connection.commit()
                mycursor.close()
                flash('Uspješno dodan novi lic certifikat')
                return redirect('https://reporter.setcor.com/licence')
            elif request.form['dodaj'] == 'Cancel':
                return redirect('https://reporter.setcor.com/licence')
        return render_template('licenceunos.html')
####################
### LICENCE EDIT ###
####################
@app.route("/uredilicence=<path:licid>", methods=['GET','POST'])
def uredilicence(licid):
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT licnaziv, licfirma, licopis, licstart, licend, licmail FROM licence where licid like %s", (licid,))
        licence=mycursor.fetchone()
        if request.method == "POST":
            licnaziv = request.form['licnaziv']
            licfirma = request.form['licfirma']
            licopis = request.form['licopis']
            licstart = request.form['licstart']
            licend = request.form['licend']
            licmail = request.form['licmail']
            mycursor.execute("UPDATE licence SET licnaziv=%s, licfirma=%s, licopis=%s, licstart=%s, licend=%s, licmail=%s WHERE licid=%s", (licnaziv, licfirma, licopis, licstart, licend, licmail, licid))
            mysql.connection.commit()
            mycursor.close()
            flash('Uspješno uređen lic certifikat')
            return redirect('https://reporter.setcor.com/licence')
        return render_template('uredilicence.html', licence=licence)
######################
### LICENCE DELETE ###
######################
@app.route("/obrisilicence=<path:licid>",methods=['GET','POST'])
def obrisilicence(licid):
        mycursor = mysql.connection.cursor()
        #mycursor.execute("SELECT * FROM korisnici where id like %s", (licid,))
        #licence=mycursor.fetchone()
        if request.method == "POST":
            if request.form['obrisi'] == 'Delete':
                #sifrakorisnika = request.form['id']
                mycursor.execute("DELETE FROM licence WHERE licid=%s", (licid, ))
                mysql.connection.commit()
                mycursor.close()
                return redirect('https://reporter.setcor.com/licence')
            elif request.form['obrisi'] == 'Cancel':
                return redirect('https://reporter.setcor.com/licence')
        return render_template('obrisilicence.html')
###############
### LICENCE ###
###############
@app.route("/licence", methods=['GET','POST'])
def licence():
        mycursor = mysql.connection.cursor()
        mycursor.execute('''SELECT licnaziv, licfirma, licopis, licstart, licend, licmail, isexpired, licid FROM licence''')
        licence = mycursor.fetchall()
        return render_template('licence.html', licence=licence)

###########
### HCP ###
###########
@app.route("/hcp", methods=['GET','POST'])
def hcp():
        mycursor = mysql.connection.cursor()
        #Uzima zadnje generirani id
        mycursor.execute("SELECT MAX(id) from hcpreport")
        lastconfig=mycursor.fetchone()
        #print(lastconfig)
        #lastminusone=int(lastconfig)-1
        #Lista sve generirane konfiguracije
        mycursor.execute("SELECT id, created FROM hcpreport ORDER BY id DESC")
        getallconfid = mycursor.fetchall()
        #print(getallconfid)
        #Dohvaca selectiranu konfu iz select forme na /vmware
        selected_config = request.form.get('config_id')
        #selectedminusone=int(selected_config)-1

        if selected_config==None:
			#ALL HCP SYSTEMS
            mycursor.execute('''select systemName , sum(objectCount), sum(ROUND(ingestedVolume/1024,2)), sum(ROUND(storageCapacityUsed/1024,2)), sum(ingestedVolume), sum(storageCapacityUsed), sum(bucketReads), sum(bucketWrites), sum(bucketDeletes) from hcp where namespaceName like "%%TOTAL" and cid like %s group by systemName;''', (lastconfig,))
            hcpsystems = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM hcpreport where id like %s''', (lastconfig, ))
            lastdate = mycursor.fetchone()
            #HCP NAMESPACE
            mycursor.execute('''select systemName, tenantName, namespaceName, objectCount, ROUND(ingestedVolume/1024,2), ROUND(storageCapacityUsed/1024,2), ingestedVolume, storageCapacityUsed, bucketReads, bucketWrites, bucketDeletes from hcp where namespaceName not like "%%TOTAL" and cid like %s;''', (lastconfig, ))
            hcpnamespace = mycursor.fetchall()
        else:
            mycursor.execute('''select systemName , sum(objectCount), sum(ROUND(ingestedVolume/1024,2)), sum(ROUND(storageCapacityUsed/1024,2)), sum(ingestedVolume), sum(storageCapacityUsed), sum(bucketReads), sum(bucketWrites), sum(bucketDeletes) from hcp where namespaceName like "%%TOTAL" and cid like %s group by systemName;''', (selected_config,))
            hcpsystems = mycursor.fetchall()
            mycursor.execute('''SELECT created FROM hcpreport where id like %s''', (selected_config, ))
            lastdate = mycursor.fetchone()
            mycursor.execute('''select systemName, tenantName, namespaceName, objectCount, ROUND(ingestedVolume/1024,2), ROUND(storageCapacityUsed/1024,2), ingestedVolume, storageCapacityUsed, bucketReads, bucketWrites, bucketDeletes from hcp where namespaceName not like "%%TOTAL" and cid like %s;''', (selected_config, ))
            hcpnamespace = mycursor.fetchall()
        return render_template('hcp.html', getallconfid=getallconfid, hcpsystems=hcpsystems, lastdate=lastdate, hcpnamespace=hcpnamespace)

@app.route("/vmwaresnapshots", methods=['GET', 'POST'])
def vmwaresnapshots():
        mycursor = mysql.connection.cursor()
        mycursor.execute("SELECT MAX(cid) from vmwaresnapshots")
        lastconfig=mycursor.fetchone()
        mycursor.execute("SELECT kid, vm, rp, folder, snapshotName, snapshotDate from vmwaresnapshots where cid like %s and rp not like 'FIREWALL_replike';", (lastconfig, ))
        snapshots=mycursor.fetchall()
        mycursor.execute('''SELECT kid, vm, rp, folder, snapshotName, snapshotDate from vmwaresnapshots where cid like %s and snapshotName like 'VEEAM BACKUP TEMPORARY SNAPSHOT';''', (lastconfig, ))
        veeamsnapshots=mycursor.fetchall()
        return render_template('vmwaresnapshots.html', snapshots=snapshots, veeamsnapshots=veeamsnapshots)
        
if __name__ == "__main__":
    app.run(debug=False,host='0.0.0.0')
