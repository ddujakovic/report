import os
import subprocess
import json
import config
import xml.etree.ElementTree as etree
from datetime import datetime
import config
import re

tokenID = config.cfg.usagemeter36

currentYM, currentY, currentM=datetime.now(), datetime.now(), datetime.now()
currentYM=currentYM.strftime("%Y%m")
currentY=currentY.strftime("%Y")
currentM=currentM.strftime("%m")

if currentM == '01':
    previousY=int(currentY)-1
    previousY=str(previousY)
    previousM=12
    previousM=str(previousM)
    previousYM=previousY+previousM
else:
    previousYM=int(currentYM)-1
    previousYM=str(previousYM)

re_kid = re.compile(r'.*(K\d\d\d\d)')
class usagemeter():
    def getcustomers(self):
            customers = []
            customers = subprocess.check_output(
                ['curl -s -X GET -k https://192.168.104.73:8443/um/api/customers -H "x-usagemeter-authorization: '+ tokenID +'"'], shell=True
            )
            root = etree.fromstring(customers)
            #print(customers)
            korisnici = []
            for child in root.iter('customer'):
                kid = child.find('name').text
                usageid = child.find('id').text
                array = {
                    'customer': kid,
                    'id': usageid
                }
                korisnici.append(array)

            return korisnici
    def getcustomerusage(self):
            data = []
            data = subprocess.run(
                    ['curl -s -X GET -k --url "https://192.168.104.73:8443/um/api/report/24?dateFrom='+previousYM+'0100&dateTo='+currentYM+'0100" -H "x-usagemeter-authorization: '+ tokenID +'" -H "Content-Type: application/json" | jq .body'], shell=True, capture_output=True,
            ).stdout.decode('utf-8')
            data = json.loads(data)
            #print(data)

            usage=[]
            for index in range(len(data)):
                for key in data[index]:
                    #print(data[index][key])
                    if key == 'customer':
                        array = {
                            'customer': str(data[index]['customer']),
                            'usageGB': str(data[index]['unitsToBeReported'])
                        }
                        usage.append(array)
            #print(usage)
            return usage   



#def main():
    #getcustomers()
    #getcustomerusage()
    #mydb.commit()


#if __name__ == "__main__":
    #main()
