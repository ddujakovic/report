###Generira mjesešni report za back-office i potrošnju cloud resursa korisnika

import mysql.connector
from mysql.connector import Error
import xlsxwriter
import config
from datetime import datetime

time=datetime.now()
time=time.strftime("%d.%m.%Y")

mydb = mysql.connector.connect(
    host=config.cfg.dbhost,
    user=config.cfg.dbuser,
    password=config.cfg.dbpass,
    database=config.cfg.database
    )


def get_kid(mycursor):
        mycursor.execute("SELECT id, korisnik FROM korisnici")
        kid = mycursor.fetchall()
        #print(kid)
        return kid

def vms_report(mycursor, kid):
        mycursor.execute("SELECT MAX(id) FROM dnevnireport")
        lastconfig=mycursor.fetchone()[0]
        #create workbook and sheet
        workbook = xlsxwriter.Workbook(time+'_capacity_report.xlsx')
        bold = workbook.add_format({'bold': True})
        #print(kid)
        for user in kid:
            korisnikid=user[0]
            korisniknaziv=user[1]
            #print(user[1])
        #VMWARE USAGE
            mycursor.execute('''SELECT rp, vm, ROUND(ram / 1024.0,2), vcpu, power_state, folder, os_type, ROUND(storage/1024/1024/1024,2), ROUND(storage_uncommited/1024/1024/1024,5) 
                            FROM vmware where cid like %s and kid like %s''', (lastconfig, korisnikid))
            result = mycursor.fetchall()

            worksheet_vms = workbook.add_worksheet(korisnikid)
                #Headers
            worksheet_vms.write(0, 0, korisniknaziv, bold)
            worksheet_vms.write(1, 0, 'Resource Pool', bold)
            worksheet_vms.write(1, 1, 'VM', bold)
            worksheet_vms.write(1, 2, 'RAM(GB)', bold)
            worksheet_vms.write(1, 3, 'VCPU', bold)
            worksheet_vms.write(1, 4, 'Power State', bold)
            worksheet_vms.write(1, 5, 'Folder', bold)
            worksheet_vms.write(1, 6, 'OS Type', bold)
            worksheet_vms.write(1, 7, 'Disk Size(GB)', bold)
            worksheet_vms.write(1, 8, 'Disk Free(GB)', bold)
            
                #Write data
            idx = 1
            for row in result:
                worksheet_vms.write(idx+1, 0, row[0])
                worksheet_vms.write(idx+1, 1, row[1])
                worksheet_vms.write(idx+1, 2, row[2])
                worksheet_vms.write(idx+1, 3, row[3])
                worksheet_vms.write(idx+1, 4, row[4])
                worksheet_vms.write(idx+1, 5, row[5])
                worksheet_vms.write(idx+1, 6, row[6])
                worksheet_vms.write(idx+1, 7, row[7])
                worksheet_vms.write(idx+1, 8, row[8])
                idx+=1
            limitvcpu=str(idx+1)
            limitram=str(idx+1)
            limitdisk=str(idx+1)
            limit=str(idx+4)
            worksheet_vms.write(idx+2, 0, 'VMWARE SUMMARY (potrebno filtrirati za poweredOn VMs)', bold)
            worksheet_vms.write(idx+3, 0, 'VCPU', bold)
            worksheet_vms.write(idx+3, 1, 'VRAM', bold)
            worksheet_vms.write(idx+3, 2, 'Disk Allocated GB', bold)
            worksheet_vms.write(idx+3, 3, 'Disk Used GB', bold)
            worksheet_vms.write(idx+3, 4, 'Disk FREE GB', bold)
            worksheet_vms.write(idx+4, 0, '{=SUBTOTAL(9,D3:D'+limitvcpu+')}' )
            worksheet_vms.write(idx+4, 1, '{=SUBTOTAL(9,C3:C'+limitvcpu+')}' )
            worksheet_vms.write(idx+4, 2, '{=SUM(H3:H'+limitdisk+')}' )
            worksheet_vms.write(idx+4, 3, '{=SUM(H3:H'+limitdisk+')-SUM(I3:I'+limitdisk+')}' )#sum=allocated-free
            worksheet_vms.write(idx+4, 4, '{=SUM(I3:I'+limitdisk+')}' )

            idy= 1
            #LUNS on HUS$
            mycursor.execute('''SELECT name, capacitygb, ROUND(consumedgb,2) FROM storageluns WHERE cid like %s and kid like %s and storage IN ('HUS VM-240270','HUS110-91257788','HUS130-92256025')''', (lastconfig, korisnikid))
            resulthus = mycursor.fetchall()
            #LUNS on OPS
            mycursor.execute('''SELECT volname, capacitygb, ROUND(consumedgb,2) FROM storages WHERE cid like %s and kid like %s''', (lastconfig, korisnikid))
            resultops = mycursor.fetchall()
            worksheet_vms.write(idy, 10, 'STORAGE SUMMARY ALL VOLUMES', bold)
            worksheet_vms.write(idy+1, 10, 'Volume name', bold)
            worksheet_vms.write(idy+1, 11, 'Volume capacity GB', bold)
            worksheet_vms.write(idy+1, 12, 'Volume used GB', bold)
            for row in resulthus:
                worksheet_vms.write(idy+2, 10, row[0])
                worksheet_vms.write(idy+2, 11, row[1])
                worksheet_vms.write(idy+2, 12, row[2])
                idy+=1
            for row in resultops:
                worksheet_vms.write(idy+2, 10, row[0])
                worksheet_vms.write(idy+2, 11, row[1])
                worksheet_vms.write(idy+2, 12, row[2])
                idy+=1
            
            #USAGEMETER
            mycursor.execute('''SELECT usagepoints FROM usagemeter WHERE cid like %s and kid like %s''', (lastconfig, korisnikid))
            usagemeterpoints = mycursor.fetchall()
            idy+=1
            worksheet_vms.write(idy+2, 10, 'VMWARE USAGE POINTS', bold)
            worksheet_vms.write(idy+3, 10, 'Usage points previous month', bold)
            for data in usagemeterpoints:
                worksheet_vms.write(idy+4, 10, data[0])
                idy+=1

            #DATASTORES
            idx=idx+8
            mycursor.execute('''SELECT name, capacitygb, freegb FROM vmwaredatastore where cid like %s and kid like %s''', (lastconfig, korisnikid))
            result = mycursor.fetchall()
            worksheet_vms.write(idx-2, 0, 'VMWARE VOLUMES', bold)
            worksheet_vms.write(idx-1, 0, 'Datastore', bold)
            worksheet_vms.write(idx-1, 1, 'Size GB', bold)
            worksheet_vms.write(idx-1, 2, 'FreeGB', bold)
            worksheet_vms.write(idx-1, 3, 'USED GB', bold)
            for row in result:
                excelrow=str(idx+1)
                worksheet_vms.write(idx, 0, row[0])
                worksheet_vms.write(idx, 1, row[1])
                worksheet_vms.write(idx, 2, row[2])
                worksheet_vms.write(idx, 3, '{=B'+excelrow+'-C'+excelrow+'}')
                idx+=1

            #VEEAM
            mycursor.execute('''select sum(ROUND(veeambckdetail.bckfilesize/1024/1024/1024)) from veeambckdetail 
            where cid like %s and kid like %s''', (lastconfig, korisnikid))
            veeambackupsize = mycursor.fetchall()
            mycursor.execute('''SELECT DISTINCT vmcount FROM veeamvmcount 
            where cid like %s and kid like %s''', (lastconfig, korisnikid))
            veeamvmcount = mycursor.fetchall()
            
            veeamrow=(idx+2)
            worksheet_vms.write(veeamrow, 0, 'VEEAM SUMMARY', bold)
            worksheet_vms.write(veeamrow+1, 0, 'Backup VMs', bold)
            worksheet_vms.write(veeamrow+1, 1, 'Backup Disk SATA GB', bold)
            for row in veeamvmcount:
                worksheet_vms.write(veeamrow+2, 0, row[0])
            for row in veeambackupsize:
                worksheet_vms.write(veeamrow+2, 1, row[0])
                veeamrow+=1
            #print("Report for user "+user+ " done...")
            
        workbook.close()

def main(mycursor):
    kid=get_kid(mycursor)
    vms_report(mycursor, kid)

if __name__ == "__main__":
    mycursor = mydb.cursor(buffered=True)
    main(mycursor)
    mydb.close()
